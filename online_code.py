# /usr/bin/python3
# coding: utf-8

"""
Online Codes implementation

block_size = (parameter)
n          = data_size / block_size
q          = 3 (parameter)
e          = 0.01 (parameter)

number of auxillary blocks: .55 * q * e * n

Outer encoding:
q auxillary blocks XOR of attached source blocks

Inner encoding:
Random distribution:
F = ceil . ln(e**2 / 4) / ln(1 - e / 2)
P1 = 1 - (1 + 1 / F) / (1 + e)
For i from 2 to F
    Pi = (1 - P1) * F / ((F - 1) * i * (i - 1))
"""

#from memory_profiler import profile, memory_usage

import sys, time, math, random, bisect
from math import ceil, log
import collections, itertools
from functools import reduce, partial
from typing import Iterable, Sequence, MutableSequence, List, Mapping, MutableMapping, Dict, Tuple, Generator, Any, Optional, NewType, cast

import numpy as np # 450ms startup
from numpy import fromstring, bitwise_xor, uint8

from utils import debug, pad_block, unpad_block, chunked, blocks_summary

SBID = NewType("SBID", int)
CBID = NewType("CBID", int)
SourceBlock = NewType("SourceBlock", bytes)
CheckBlock = NewType("CheckBlock", bytes)


#######################################################################
# Utilities for codec                                                 #
#######################################################################


def coroutine(func):
    def start(*args, **kwargs):
        c = func(*args, **kwargs)
        next(c)
        return c
    return start

def updateTimings(tl, tn, ncb, nsb):
    tn = time.time()
    if tn > tl + .005:
        print("c%ss%s" % (ncb, nsb), end=' ', flush=True)
        tl = tn
    return tl, tn

def printAdjacency(adj:Dict[CBID, Sequence[SBID]]):
    #debug(dict.__repr__(adj))
    debug('{' + " ".join("[%s:%s]" % (k, ' '.join(map(str, v))) for k, v in adj.items()) + '}')

def printRate(sb, cb):
    if not sb:
        print("\nRedundancy: 0 \nRate: 0")
    else:
        print("\nRedundancy:", len(cb), '/', len(sb), '=', len(cb) / len(sb))
        print("Rate:", len(sb), '/', len(cb), '=', len(sb) / len(cb))

def sample(pop: Sequence[Any], size: Optional[int], replace=True, pdf: Optional[Sequence[float]] = None) -> \
Sequence[Any]:
    if not replace and size and size > len(pop): return pop
    return np.random.choice(pop, size, replace=replace, p=pdf)

def sample_unique(pop:Sequence[Any], size:int, prng=random) -> Sequence[Any]:
    if size > len(pop): return pop
    #result = random.sample(pop, size)  # Incompatible with np.ndarray sadly...
    # Adapted from CPython random.sample
    randbelow = prng.randrange
    n = len(pop)
    result = [None] * size
    setsize = 21  # size of a small set minus size of an empty list
    if size > 5:
        setsize += 4 ** ceil(log(size * 3, 4))  # table size for big sets
    if n <= setsize:
        # An n-length list is smaller than a k-length set
        pool = list(pop)
        for i in range(size):  # invariant:  non-selected at [0,n-i)
            j = randbelow(n - i)
            result[i] = pool[j]
            pool[j] = pool[n - i - 1]  # move non-selected item into vacancy
    else:
        selected = set()
        selected_add = selected.add
        for i in range(size):
            j = randbelow(n)
            while j in selected:
                j = randbelow(n)
            selected_add(j)
            result[i] = pop[j]
    # print("sample_unique:", result)
    return result

def choose_degree(cdf:Sequence[float], prng=random, _search_func=bisect.bisect_left) -> int:
    idx = _search_func(cdf, prng.random()) + 1
    # print("choose_degree:", idx)
    return idx

def xor_blocks_np(blocks:Iterable[np.ndarray]) -> np.ndarray:  # Functional style: a bit faster
    return reduce(bitwise_xor, blocks)

def xor_blocks(blocks:Iterable[bytes]) -> bytes:  # Functional style: a bit faster
    return reduce(bitwise_xor, (fromstring(blk, dtype=uint8) for blk in blocks)).tostring()


#######################################################################
# Codec implementation                                                #
#######################################################################


class CompositeOnlineCodec:
    """Online Codes implementation that uses auxilary blocks strategy and can encode/decode with a fixed parameter N."""

    def __init__(self, n:int, q:int=3, e:float=.01, f:int=0, aseed:int=1, dseed:int=1) -> None:
        """
        N could be optional to the codec, but several properties rely on it to improve efficiency
        Rate increases with high block degrees predominancy
        Algorithm works faster with fewer blocks (n is way more important than block size)
        """
        assert q > 0 and e > 0
        self.n = n  # number of blocks
        self.q = q  # quality
        self.e = e  # efficiency (suboptimality)

        self.f = f or int(log(e ** 2 / 4) / log(1 - e / 2))
        self.compute_distribution()
        print("q:", self.q, "e:", self.e, "F:", self.f, "P1:", self.p[0])
        self.adjacency_prng = random.Random()
        self.degree_prng = random.Random()
        self.seed(aseed, dseed)

        # Precompute to speed up algorithm
        self.degrees = np.arange(1, self.f + 1)
        self.indices_aux = list(range(self.auxillary_blocks_number()))
        self.indices_src = range(n + self.auxillary_blocks_number())

        # For debugging purpose
        self.degree_history = []  # type: List[int]

    def compute_distribution(self):
        # Compute online code soliton distribution
        # TODO?: implement robust soliton distribution
        f, n = self.f, self.n
        p = np.empty(f, dtype=np.float32)
        p1 = 1 - (1 + 1 / f) / (1 + self.e)  # n**-.5
        p[0] = p1
        for i in range(1, f):
            p[i] = (1 - p1) * f / ((f - 1) * (i + 1) * (i))
        # print("Distribution:", p)
        # with open("online_distribution_%s_%s" % (e, n), "w") as file: file.write('\n'.join(str(pp) for pp in p))
        self.p = p
        self.cdf = list(itertools.accumulate(p))
        assert math.isclose(sum(p), self.cdf[-1], abs_tol=1e-5) and math.isclose(self.cdf[-1], 1., abs_tol=1e-5)

    def seed(self, aseed:int=1, dseed:int=1):
        self.adjacency_prng_seed = aseed or np.random.randint(0, 2 ** 32)
        self.degree_prng_seed = dseed or np.random.randint(0, 2 ** 32)
        print("Seeds:", [self.adjacency_prng_seed, self.degree_prng_seed])

    def auxillary_blocks_number(self) -> int:
        return int(ceil(.55 * self.q * self.e * self.n))

    def min_needed_output_blocks(self) -> int:
        return int(ceil((1 + self.e) * (self.n + self.auxillary_blocks_number())))

    def gen_cbid(self) -> Generator[CBID, None, None]:
        yield from map(CBID, itertools.count())

    sample = staticmethod(sample)
    sample_unique = staticmethod(sample_unique)
    choose_degree = staticmethod(choose_degree)

    def degree_from(self, seed:int):
        # Cannot precompute degrees because must seed with CBID
        random.seed(self.degree_prng_seed + seed)
        # 1 choice in [1, F] given probability density function P
        d = self.choose_degree(cdf=self.cdf)
        self.degree_history.append(d)
        return d

    def encoder(self, source_blocks:MutableSequence[SourceBlock]) -> Generator[Tuple[CBID, CheckBlock], None, None]:
        """Generator encoder for online codes.
        It takes source blocks as parameter and produces an infinite iterator of check blocks.
        It mutates the sequence of source blocks.
        """
        q, n, n_aux = self.q, len(source_blocks), self.auxillary_blocks_number()
        aprng, dprng = self.adjacency_prng, self.degree_prng
        source_blocks = [fromstring(sb, dtype=uint8) for sb in source_blocks]
        print("N aux:", n_aux)

        # Outer encoding

        aux_adjacencies = collections.defaultdict(list)  # type: Dict[int, List[int]]
        aprng.seed(self.adjacency_prng_seed)
        for sbid in range(n):
            for abid in self.sample_unique(self.indices_aux, q, prng=aprng):
                aux_adjacencies[abid].append(sbid)
        for abid, aux_adj in aux_adjacencies.items():
            aux_block = xor_blocks_np([source_blocks[abid_] for abid_ in aux_adj])
            source_blocks.append(SourceBlock(aux_block))
            # debug("Auxillary block:", abid, len(aux_adj))#, aux_block.tostring())

        # (WRONG COMPUTATION!!!)
        # for abid in range(n_aux):
        #     src_indices = self.sample_unique(self.indices_aux, q, prng=aprng)
        #     aux_block = xor_blocks([source_blocks[src_idx] for src_idx in src_indices])
        #     #debug("Auxillary block:", abid, list(src_indices), [b.tostring() for b in source_blocks[n:]])
        #     source_blocks.append(SourceBlock(aux_block))

        # debug("Auxillary blocks:", n_aux, [b.tostring() for b in source_blocks[n:]])
        debug()

        cbid_gen = self.gen_cbid()
        while True:
            # Inner encoding
            cbid = next(cbid_gen)
            #debug("CBID", cbid, end=' ')

            dprng.seed(self.degree_prng_seed + cbid)
            d = self.choose_degree(cdf=self.cdf, prng=dprng)  # type: int
            self.degree_history.append(d)

            aprng.seed(self.adjacency_prng_seed + cbid)
            src_indices = self.sample_unique(self.indices_src, d, prng=aprng)

            check_block = xor_blocks_np([source_blocks[idx] for idx in src_indices])
            # debug(cbid, "adjacency", d, list(src_indices))
            # debug(" ", check_block)
            yield (cbid, CheckBlock(check_block.tostring()))

    @coroutine
    def decoder(self) -> Generator[Dict[SBID, SourceBlock], MutableMapping[CBID, CheckBlock], None]:
        """Generator decoder for online codes.
        It takes input for each step with the send(object) method of generator objects,
        and produce decoded blocks or None if there is not enough data to decode blocks.
        The coroutine decorator applied on this function bootstraps the generator on creation.
        """
        n, q = self.n, self.q

        recovered_blocks = {}  # type: Dict[SBID, np.ndarray]
        check_blocks = {}  # type: Dict[CBID, np.ndarray]
        adjacencyCB = collections.defaultdict(list)  # type: Dict[CBID, List[SBID]]
        adjacencySB = collections.defaultdict(list)  # type: Dict[SBID, List[CBID]]
        one_degree = []
        n_sb_recovered = 0

        known_other_cb_filter = lambda cbid: cbid in check_blocks.keys() and cbid != current_cbid
        aprng, dprng = self.adjacency_prng, self.degree_prng

        aprng.seed(self.adjacency_prng_seed)
        for sbid in range(n):
            aux_adj = self.sample_unique(self.indices_aux, q, prng=aprng)
            for abid in aux_adj:
                adjacencyCB[-(n + abid)].append(sbid)
            adjacencySB[sbid] = [-(n + abid_) for abid_ in aux_adj]

        # aprng.seed(self.adjacency_prng_seed)
        # for i in range(self.auxillary_blocks_number()):
        #     adj = list(self.sample_unique(self.indices_aux, q, prng=aprng))
        #     for sbid in adj:
        #         adjacencySB[sbid].append(CBID(-(n + i)))
        #     adjacencyCB[CBID(-(n + i))] = adj

        # debug("Initial adjacency map:")
        # printAdjacency(adjacencySB)
        # printAdjacency(adjacencyCB)

        while n_sb_recovered != n:
            # Get next free check block, if there is none, yield and wait for more
            if not one_degree:
                more_check_blocks = yield None  # Means "I cannot fully decode yet! Gimme more!"
                if more_check_blocks is None: break
                # debug("\nIncoming check blocks:", list(more_check_blocks))
                check_blocks.update((cbid, fromstring(cb, dtype=uint8)) for cbid, cb in more_check_blocks.items())

                # Update adjacency map
                for cbid in more_check_blocks.keys():
                    dprng.seed(self.degree_prng_seed + cbid)
                    d = self.choose_degree(cdf=self.cdf, prng=dprng)  # type: int
                    self.degree_history.append(d)

                    aprng.seed(self.adjacency_prng_seed + cbid)
                    adj = [SBID(sbid) for sbid in self.sample_unique(self.indices_src, d, prng=aprng)]  # type: List[SBID]
                    # debug("Adjacency before reduction:", adj)
                    for sbid in adj[:]:
                        if sbid in recovered_blocks:
                            # debug("Removing already recovered", sbid)
                            adj.remove(sbid)
                            check_blocks[cbid] = xor_blocks_np([check_blocks[cbid], recovered_blocks[sbid]])
                        else:
                            adjacencySB[sbid].append(cbid)
                    adjacencyCB[cbid] = adj
                    if len(adj) == 1: one_degree.append(cbid)  # ; debug("Found one_degree")
                continue

            current_cbid = one_degree.pop()
            current_sbid = adjacencyCB[current_cbid].pop()
            # debug("To recover (SBID, CBID):", current_sbid, current_cbid)

            # This is inner + outer decoding, because auxillary blocks become check blocks when found
            # debug("Adjacency map before reduction:")
            # printAdjacency(adjacencyCB)
            # printAdjacency(adjacencySB)
            recovered_block = check_blocks[current_cbid]
            # Get all CB that are known and adjacent to SB
            # debug("CBIDs:", [current_cbid] + list(filter(known_other_cb_filter, adjacencySB[current_sbid])))
            for cbid in filter(known_other_cb_filter, adjacencySB[current_sbid]):  # Reduce adjacency
                check_blocks[cbid] = xor_blocks_np([check_blocks[cbid], recovered_block])
                adjacencyCB[cbid].remove(current_sbid)
                cb_len = len(adjacencyCB[cbid])
                if cb_len == 1:
                    one_degree.append(cbid)  # ; debug("Found one_degree")
                elif cb_len == 0:
                    one_degree.remove(cbid)
            adjacencySB[current_sbid].clear()  # or leave it as is?

            # Store depending on whether block must be processed further
            if current_sbid in recovered_blocks:
                print("/!\ Already recovered:", current_sbid)
            elif current_sbid < n:
                recovered_blocks[current_sbid] = SourceBlock(recovered_block)
                n_sb_recovered += 1
                # debug("Recovered source block:", current_sbid, "n:", n_sb_recovered)
            else:
                # debug("Found auxillary block:", current_sbid, "->", -current_sbid)
                cbid_aux = cast(CBID, -current_sbid)
                check_blocks[cbid_aux] = CheckBlock(recovered_block)
                recovered_blocks[current_sbid] = SourceBlock(recovered_block)  # Only to determine if known
            # debug("Recovered blocks:", len(recovered_blocks.keys()), list(recovered_blocks.keys()))

        printRate(recovered_blocks, adjacencyCB)
        yield {sbid: sb.tostring() for sbid, sb in recovered_blocks.items() if sbid < n}


class OnlineCodec:
    """Online codec implementation that is generic, multiplexed (multi objects) and flow-oriented (progressive encoding/decoding)."""

    def __init__(self, e:float=.01, base_seed:int=1, f:int=0) -> None:
        assert e > 0
        self.e = e  # efficiency (suboptimality)

        self.f = f or int(log(e ** 2 / 4) / log(1 - e / 2))
        self.p = None  # Initialized after
        self.adjacency_prng = random.Random()
        self.degree_prng = random.Random()
        self.base_seed = None  # Initialized after
        self.compute_distribution()
        print("e:", self.e, "F:", self.f, "P1:", self.p[0], end=' ')
        self.seed(base_seed)

        # For debugging purpose
        self.degree_history = []  # type: List[int]

    def compute_distribution(self):
        """Compute online codes random distribution as defined by Maymounkov, 
analogous to Luby's "soliton distribution". """
        f = self.f
        self.p = p = np.empty(f, dtype=np.float32)
        # Suite is starts at 1, Python list indices at 0, so we adapt formula
        p[0] = p1 = 1 - (1 + 1 / f) / (1 + self.e)
        for i in range(1, f):
            p[i] = (1 - p1) * f / ((f - 1) * (i + 1) * (i))
        # print("Distribution:", p)
        # with open("online_distribution_%s_%s" % (e, n), "w") as file: file.write('\n'.join(str(pp) for pp in p))
        # 1 choice in [1, F] given probability density function P
        self.cdf = list(itertools.accumulate(p))
        # print("Distribution validity:", sum(p), self.cdf[-1])
        assert math.isclose(sum(p), self.cdf[-1], abs_tol=1e-5) and math.isclose(self.cdf[-1], 1., abs_tol=1e-5), "Distribution is invalid!"

    def seed(self, base_seed:int=1):
        """Sets the seed used by PRNGs in the codec. If falsy the seed is chosen randomly."""
        self.base_seed = base_seed or random.randrange(2 ** 32)
        print("seed:", self.base_seed)

    def auxillary_blocks_number(self) -> int:
        return 0

    def min_needed_output_blocks(self, n:int) -> int:
        """Minimum number of blocks needed de decode, as per 'Maymounkov - Online Codes and Big Downloads'"""
        return int(ceil((1 + self.e) * n))

    def gen_cbid(self) -> Generator[CBID, None, None]:
        """Returns a unique check block id taken from an infinite counter."""
        yield from map(CBID, itertools.count())

    # internalize some utility methods
    sample = staticmethod(sample)
    sample_unique = staticmethod(sample_unique)
    choose_degree = staticmethod(choose_degree)

    def compute_adjacency(self, variable_seed:int, n:int):
        """Computes adjacency of a block based on current corpus size `n` and a variable part of the seed used to synchronize the PRNGs. Returns a list of block indices (which are also block ids). `variable_seed` is usually a check block id."""
        aprng, dprng = self.adjacency_prng, self.degree_prng

        dprng.seed(self.base_seed + variable_seed)
        d = self.choose_degree(cdf=self.cdf, prng=dprng)  # type: int
        self.degree_history.append(d)

        aprng.seed(self.base_seed + variable_seed)
        adj = self.sample_unique(range(n), d, prng=aprng)

        return adj

    @coroutine
    def encoder(self) -> Generator[List[Tuple[CBID, int, CheckBlock]], Mapping[SBID, SourceBlock], None]:
        """Encoder coroutine for online codes, that allow buffered stream-like encoding.

        Consumers must send it (nb_iter, source_blocks), where
        `nb_iter` indicates how many iterations to perform before yielding newly generated check blocks;
        `source_blocks` is an iterable of (sbid, source_block) pairs to buffer.

        (0, source_blocks) signals the coroutine to pause generation of check blocks.
        `source_blocks` is buffered if truthy.

        Yields a list of (cbid, n, check_block)-uplets at each step, containing generated check blocks (=symbols).

        The coroutine decorator applied on this generator function bootstraps the generator on creation.
        """
        source_blocks = {}  # type: Dict[SBID, SourceBlock]
        generated_blocks = []  # type: List[Tuple[int, CBID, CheckBlock]]
        cbid_gen = self.gen_cbid()
        nb_iter = 0

        while True:
            while nb_iter == 0:
                nb_iter, more_source_blocks = yield generated_blocks
                generated_blocks = []
                if more_source_blocks:
                    source_blocks.update(more_source_blocks)
                    n = len(source_blocks)

            cbid = next(cbid_gen)
            # Requires that all N first blocks are available
            adj = self.compute_adjacency(cbid, n)
            # blocks_summary(source_blocks)
            # debug("cbid", cbid, "n", n, "adjacency", list(adj))
            check_block = xor_blocks([source_blocks[idx] for idx in adj])
            generated_blocks.append((cbid, n, CheckBlock(check_block)))

            nb_iter -= 1


    @coroutine
    def decoder(self) -> Generator[Dict[SBID, SourceBlock], List[Tuple[CBID, int, CheckBlock]], None]:
        """Decoder coroutine for online codes, that allows buffered stream-like decoding.

        Consumers must send it (nb_iter, check_blocks), where
        `nb_iter` indicates how many iterations (at most) to perform before yielding newly recovered source blocks;
        `check_blocks` is an iterable of (cbid, n, check_block) pairs that are fed to the decoding step.
        (0, whatever) signals the coroutine to pause.

        It yields a dictionnary of (sbid, source_block) key-values at each step, containing the latest recovered blocks.

        The coroutine decorator applied on this generator function bootstraps the generator on creation.
        """
        recovered_blocks = {}  # type: Dict[SBID, np.ndarray]
        check_blocks = {}  # type: Dict[CBID, np.ndarray]
        adjacencyCB = collections.defaultdict(list)  # type: Dict[CBID, List[SBID]]
        adjacencySB = collections.defaultdict(list)  # type: Dict[SBID, List[CBID]]
        one_degree = []  # type: List[CBID]
        recent_blocks = []  # type: List[SBID]

        known_other_cb_filter = lambda cbid: cbid in check_blocks.keys() and cbid != current_cbid
        # tl = tn = 0.
        # tl, tn = updateTimings(tl, tn, len(adjacencyCB), len(recovered_blocks))

        while True:
            # Get next free check block, if there is none, yield and wait for more
            nb_iter, more_check_blocks = yield {sbid: recovered_blocks[sbid].tostring() for sbid in recent_blocks}
            if nb_iter == 0:
                printRate(recovered_blocks, adjacencyCB)
                continue
            recent_blocks = []  # type: List[SourceBlock]
            # debug("Incoming check blocks:", more_check_blocks, "max_itertions:", nb_iter)
            check_blocks.update((cbid, fromstring(cb, dtype=uint8)) for cbid, _, cb in more_check_blocks)

            # Update adjacency map
            for cbid, n, _ in more_check_blocks:
                adj = [SBID(sbid) for sbid in self.compute_adjacency(cbid, n)]  # type: List[SBID]
                # debug("Adjacency before reduction:", adj)
                for sbid in adj[:]:
                    if sbid in recovered_blocks:
                        # debug("Removing already recovered", sbid)
                        adj.remove(sbid)
                        check_blocks[cbid] = xor_blocks_np([check_blocks[cbid], recovered_blocks[sbid]])
                        # debug("Reduced:", cbid, "using", sbid)
                    else:
                        adjacencySB[sbid].append(cbid)
                # debug("Adjacency after reduction:", adj)
                adjacencyCB[cbid] = adj
                if len(adj) == 1:
                    one_degree.append(cbid)
                    # debug("Found one_degree")

            while one_degree and nb_iter > 0:
                current_cbid = one_degree.pop()
                current_sbid = adjacencyCB[current_cbid].pop()
                # debug("\nTo recover (SBID, CBID):", current_sbid, current_cbid)

                # Decoding and reduction
                recovered_block = check_blocks.pop(current_cbid)
                # Get all CB that are known and adjacent to SB
                for cbid in filter(known_other_cb_filter, adjacencySB[current_sbid]):  # Reduce adjacency
                    check_blocks[cbid] = xor_blocks_np([check_blocks[cbid], recovered_block])
                    adjacencyCB[cbid].remove(current_sbid)
                    # debug("Reduced:", cbid)
                    cb_len = len(adjacencyCB[cbid])
                    if cb_len == 1:
                        one_degree.append(cbid)
                        # debug("Found one_degree")
                    elif cb_len == 0:
                        one_degree.remove(cbid)
                        del check_blocks[cbid]
                        # debug("Removed one_degree")
                adjacencySB[current_sbid].clear()  # or leave it as is?

                recovered_blocks[current_sbid] = SourceBlock(recovered_block)
                recent_blocks.append(current_sbid)
                # debug("Recovered source block:", current_sbid)#, recovered_block.tostring(), "n:", len(recovered_blocks))
                nb_iter -= 1
            # tl, tn = updateTimings(tl, tn, len(adjacencyCB), len(recovered_blocks))
            # debug("Recovered blocks:", len(recovered_blocks.keys()), list(recovered_blocks.keys()))


#######################################################################
# Informal tests                                                      #
#######################################################################


def testEncode(codec:CompositeOnlineCodec, blocks:MutableSequence[SourceBlock]) -> Dict[CBID, CheckBlock]:
    """For informal tests purpose. Performs encoding using a CompositeOnlineCodec."""
    symbols = {}
    encoder = codec.encoder(blocks)
    enough = codec.min_needed_output_blocks()
    for cbid, sym in itertools.islice(encoder, int(enough * 2)):
        symbols[cbid] = sym
    del encoder
    return symbols

def testDecode(codec:CompositeOnlineCodec, symbols:MutableMapping[CBID, CheckBlock]) -> Dict[SBID,SourceBlock]:
    """For informal tests purpose. Performs decoding using a CompositeOnlineCodec."""
    decoder = codec.decoder()
    decoded = None  # type: Optional[Dict[SBID, SourceBlock]]
    while not decoded:
        k = min(len(symbols), 1)
        symbolsFeed = [symbols.popitem() for _ in range(k)]  # type: Tuple[CBID, CheckBlock]
        if not symbolsFeed: decoded = decoder.send(None)
        else: decoded = decoder.send(dict(symbolsFeed))
    del decoder
    return decoded

def test():
    """Test for composite online codec (with auxillary blocks).
Working example of encoding/decoding using CompositeOnlineCodec."""
    # TODO (done): profile and optimize random choice
    # TODO (done): change distribution computation, data structure (float array)
    # TODO (done): test different block segmentations (-> n bigger impact than bs)
    # TODO (done): test different q, epsilon (must vary according to n)
    # TODO:(done): complexity analysis of algorithm (number of calls, iterations)
    # TODO (done): are the auxillary blocks used efficiently? no
    # TODO: compare with other implementations

    import cProfile
    from copy import deepcopy
    import gc

    with open("test_files/100Mio", "rb") as file:
        data = file.read()

    data_size = len(data)
    block_size = 8*1024
    n = int(ceil(data_size / block_size))
    print("l:", data_size, "n:", n, "bs:", block_size)
    blocks = [SourceBlock(data[i * block_size: i * block_size + block_size]) for i in range(n)]
    blocks[-1] = SourceBlock(pad_block(blocks[-1], block_size))

    codec = CompositeOnlineCodec(n, q=3, e=.01, aseed=0, dseed=0)#, f=200)
    print("Minimum check blocks needed:", codec.min_needed_output_blocks())

    for i in range(1): #30
        print()
        codec.seed(0, 0)

        t0 = time.time()
        symbols = testEncode(codec, deepcopy(blocks))
        print("Encoding duration:", time.time() - t0)
        #cProfile.run("testEncode(codec, blocks)", sort="time")

        from statistics import mean, median
        c = collections.Counter(codec.degree_history)
        print("\nDegrees:", (1, c[1]), c.most_common(6), "…")
        print("Total, Mean, Median of degrees:", sum(c.values()), mean(list(c.elements())), median(list(c.elements())))

        print()

        # loss_rate = .05#round(np.random.random() * .5, 2)
        # lost_cbid_list = np.random.choice(list(symbols.keys()), int(loss_rate * len(symbols)), replace=False)
        # print("Lost {}% symbols: {} -> {}\n".format(loss_rate*100, len(symbols), (len(symbols) - len(lost_cbid_list))))
        # for lost_cbid in lost_cbid_list:
        #     del symbols[lost_cbid]

        t0 = time.time()
        decoded = testDecode(codec, symbols)
        print("Decoding duration:", time.time() - t0)
        #cProfile.run("testDecode(codec, symbols)", sort="time")
        #debug("\nDecoded:", decoded)

        if decoded:
            reconstructed = unpad_block(b''.join((decoded[k] for k in sorted(decoded.keys()))), data_size)
            # debug('\n', reconstructed, '\n')
            print("Is complete:", data == reconstructed)
            if data != reconstructed:
                corrupt = {}
                debug("Lengths:", len(decoded), len(blocks))
                for i, k in enumerate(decoded):
                    if decoded[k] != blocks[i]:
                        corrupt[k] = decoded[k]
                #debug("Corrupt blocks:", list(corrupt))
            del reconstructed
        else:
            print("Decoding failed")
        del symbols
        del decoded
        # debug("GC:", gc.collect())

    del data
    del blocks
    del codec

def testEncode2(codec:OnlineCodec, blocks:MutableSequence[SourceBlock]) -> List[Tuple[CBID, int, CheckBlock]]:
    """For informal tests purpose. Performs encoding using a OnlineCodec."""
    encoder = codec.encoder()
    enough = codec.min_needed_output_blocks(len(blocks))
    symbols = encoder.send((int(enough * 2), {bid: blk for bid, blk in enumerate(blocks)}))
    #memory_usage((encoder.send, ((int(enough * 2), {bid: blk for bid, blk in enumerate(blocks)}))))
    #symbols = encoder.send((0, None))
    encoder.close()
    return symbols

def testDecode2(codec:OnlineCodec, symbols:List[Tuple[CBID, int, CheckBlock]], corpus_size:int) -> Dict[SBID, SourceBlock]:
    """For informal tests purpose. Performs decoding using a OnlineCodec."""
    decoder = codec.decoder()
    decoded_blocks = {}  # type: Optional[Dict[SBID, SourceBlock]]
    try:
        for symbolFeed in chunked(10, symbols):
            decoded = decoder.send((10**10, symbolFeed))
            decoded_blocks.update(decoded)
            if len(decoded_blocks) >= corpus_size:
                break
        decoded = decoder.send((0, None))
        decoded_blocks.update(decoded)
    except StopIteration:
        print("Decoding finished")
    decoder.close()
    return decoded_blocks

def test2():
    """Test for online codec (no auxillary blocks).
Working example of encoding/decoding using OnlineCodec."""
    import gc

    # Tests Parameters
    with open("test_files/100Mio", "rb") as file:
        data = file.read()
    data_size = len(data)
    block_size = 8192
    n = int(ceil(data_size / block_size))
    print("l:", data_size, "n:", n, "bs:", block_size)
    
    # Basic block segmentation
    blocks = [SourceBlock(data[i * block_size: i * block_size + block_size]) for i in range(n)]
    blocks[-1] = SourceBlock(pad_block(blocks[-1], block_size))

    # Codec instanciation
    codec = OnlineCodec(e=.01, base_seed=0)#, f=200)
    print("Minimum check blocks needed:", codec.min_needed_output_blocks(n))

    # Does tests arbitrary number of times
    for i in range(1):
        print()
        codec.seed(0)

        t0 = time.time()
        symbols = testEncode2(codec, blocks)
        print("Encoding duration:", time.time() - t0)

        # Display some statistics
        from statistics import mean, median
        c = collections.Counter(codec.degree_history)
        print("\nDegrees:", (1, c[1]), c.most_common(6), "…")
        print("Total, Mean, Median of degrees:", sum(c.values()), mean(list(c.elements())), median(list(c.elements())))

        print()

        # Emulate packet loss
        # loss_rate = .0005#round(np.random.random() * .5, 2)
        # lost_cbid_list = np.random.choice(range(len(symbols)), int(loss_rate * len(symbols)), replace=False)
        # print("Lost {}% symbols: {} -> {}\n".format(loss_rate*100, len(symbols), (len(symbols) - len(lost_cbid_list))))
        # symbols = [s for i, s in enumerate(symbols) if i not in lost_cbid_list]

        t0 = time.time()
        decoded = testDecode2(codec, symbols, n)
        print("Decoding duration:", time.time() - t0)
        #debug("Decoded:", decoded)

        if decoded:
            reconstructed = unpad_block(b''.join((decoded[k] for k in sorted(decoded.keys()))), data_size)
            print("Is complete:", data == reconstructed)
            del reconstructed
        else:
            print("Decoding failed")
        del symbols
        del decoded
        # print("GC:", gc.collect())

    del data
    del blocks
    del codec

if __name__ == "__main__":
    # If executed, launch test 
    test()
