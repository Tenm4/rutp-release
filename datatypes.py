# /usr/bin/python3
# coding: utf-8

"""
Reliable Unidirectional Transfer Protocol - prototype implementation

Datatypes
"""

from typing import List, Tuple, MutableSequence, Dict, Text, Union, Optional, Any


class ObjectMetadata: # aka formatted object

    __slots__ = ("oid", "size", "priority", "uri", "data", "hash", "blocks", "symbols")
    displayable_attrs = ("oid", "size", "priority", "uri", "data")

    def __init__(self, oid:int=-1, uri:Text="raw", size:int=0, priority:int=0, data:bytes=None, hash_:bytes=b'\xFF' * 32) -> None:
        self.oid = oid # not an URI because very inefficient to transmit in every packet
        self.uri = uri # raw, file:, net:
        self.size = size
        self.priority = priority
        self.data = data
        self.hash = hash_
        # Temporary attributes used while processing
        self.blocks = [] # type: MutableSequence[BlockMetadata]
        self.symbols = [] # type: MutableSequence[SymbolMetadata]

    def __repr__(self):
        return "Object" + repr(tuple(getattr(self, attr, None) for attr in self.displayable_attrs))

    def __eq__(self, other):
        return self.oid == other.oid

    def __hash__(self):
        return hash(self.oid)

class BlockMetadata: # aka formatted block

    __slots__ = ("bid", "oid", "priority", "data")

    def __init__(self, bid:int=-1, oid:int=-1, priority:int=0, data:bytes=None) -> None:
        self.bid = bid # 0: metadata, >1: data
        self.oid = oid
        self.priority = priority
        self.data = data

    def __repr__(self) -> str:
        return "Block" + repr(tuple(getattr(self, attr, None) for attr in self.__slots__))

    def __eq__(self, other):
        return self.oid == other.oid and self.bid == other.bid

    def __hash__(self):
        return hash((self.oid, self.bid))

class SymbolMetadata: # aka formatted symbol

    __slots__ = ("sid", "oid", "size", "data", "crc")

    def __init__(self, sid:int=-1, oid:int=-1, size:int=0, data:bytes=None, crc:bytes="\0"*4) -> None:
        self.sid = sid
        self.oid = oid
        self.size = size
        self.data = data
        self.crc = crc

    def __repr__(self) -> str:
        return "Symbol" + repr(tuple(getattr(self, attr, None) for attr in self.__slots__))

    def __eq__(self, other):
        return self.oid == other.oid and self.sid == other.sid

    def __hash__(self):
        return hash((self.oid, self.sid))
