# /usr/bin/python3
# coding: utf-8

"""
Online Codes minimal integration for testing purposes

Minimal and dirty RUTP client/servers scripts.
Implements minimal functionality of all RUTP elements, including latest FEC implementation. To merge with main RUTP implementation.

RUTP does not implement congestion control as it is designed mainly for diode links. 
Packet drop is in general mostly caused by congestion rather than processing errors.
RUTP is effectively a transport-layer protocol, implemented here on top of UDP. 
UDP checksum is not employed.
"""

#from memory_profiler import profile, memory_usage

import sys, math, time, socket, struct
import collections, itertools
from typing import Iterable, Sequence, MutableSequence, List, Mapping, MutableMapping, Dict, Tuple, Generator, Any, Optional, NewType, cast
from statistics import mean, median

from utils import pad_block, unpad_block, hash256, crc32, tohex, convert
from online_code import OnlineCodec, SourceBlock, CheckBlock, SBID, CBID


FILENAME = "test_files/100Mio" #16Kio"
BLOCK_SIZE = 8*1024
CODEC_PARAMS = dict(e=.01, base_seed=1)
SOURCE_REDUNDANCY = 2.
SOCK_SLEEP_TIME = .000001
SOCK_TIMEOUT = 2.

PDU_BASE_FMT = ">BI"                          # status, nb_sent
DATA_FMT = ">QI{}s".format(BLOCK_SIZE)        # sid, n, symbol_data
METADATA_FMT = ">Q32s"                        # object_size, hash_256
PDU_DATA_FMT = ">B" + DATA_FMT[1:]
PDU_DATA_SIZE = struct.calcsize(PDU_DATA_FMT)
PDU_BASE_SIZE = struct.calcsize(PDU_BASE_FMT)
METADATA_SIZE = struct.calcsize(METADATA_FMT)
NET_BUFFER_SIZE = PDU_DATA_SIZE


def client(ip, port):
    # Get data
    with open(FILENAME, "rb") as file:
        data = file.read()

    # Initialize parameters
    data_size = len(data)
    N = int(math.ceil(data_size / BLOCK_SIZE))
    print("Message length:", data_size, "N:", N, "bs:", BLOCK_SIZE)

    # Metadata
    hash = hash256(data)
    metadata_pdu = pad_block(struct.pack(METADATA_FMT, data_size, hash), BLOCK_SIZE)
    print("Metadata:", data_size, tohex(hash))

    # Segmentation
    blocks = [metadata_pdu] + [data[i * BLOCK_SIZE: i * BLOCK_SIZE + BLOCK_SIZE] for i in range(N)]
    blocks[-1] = pad_block(blocks[-1], BLOCK_SIZE)
    blocks = enumerate(blocks) # not dict because ordering is not defined in Python specification
    N += 1  # For metadata

    # Codec setup
    print("Initializing error correcting codec: Online Code")
    codec = OnlineCodec(**CODEC_PARAMS)
    enough = codec.min_needed_output_blocks(N)
    print("Minimum check blocks needed:", enough)
    encoder = codec.encoder()
    # encoder.send((int(enough * SOURCE_REDUNDANCY), {bid: blk for bid, blk in enumerate(blocks)}))
    # encoder_output = encoder.send((0, None))

    # Networking setup
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    
    sock.sendto(struct.pack(PDU_BASE_FMT, 0, 0), (ip, port))
    print("RUTP START")

    # Variables
    symbols = {}

    # Emission
    print("Beginning emission to UDP %s:%s" % (ip, port))
    t0 = time.time()
    stop = False
    while not stop:
        try:
            # First, feed source blocks progressively
            # bid, blk = blocks.popitem() # Man, this was dependent on internal dict ordering... 
            bid, blk = next(blocks)
            encoder_res = encoder.send((1, [(bid, blk)]))
        except StopIteration:
            if len(symbols) < int(enough * SOURCE_REDUNDANCY):
                # Then, go on and generate further symbols until we reach max redundancy
                encoder_res = encoder.send((1, None))
            else:
                # Finally, stop the codec
                encoder_res = encoder.send((0, None))
                stop = True
        # input() # debug
        for sid, n, symbol in encoder_res:
            # Send every symbol generated
            symbols[sid] = symbol
            pdu = struct.pack(PDU_DATA_FMT, 2, sid, n, symbol)
            # print("Sent data:", sid, n)#, symbol)
            sock.sendto(pdu, (ip, port))
            time.sleep(SOCK_SLEEP_TIME)
    encoder.close()
    del encoder
    print("Emission duration:", time.time() - t0)

    c = collections.Counter(codec.degree_history)
    print("\nDegrees:", (1, c[1]), c.most_common(6), "…")
    print("Total, Mean, Median of degrees:", sum(c.values()), mean(list(c.elements())), median(list(c.elements())))
    del codec

    print("RUTP END", len(symbols))
    sock.sendto(struct.pack(PDU_BASE_FMT, 1, len(symbols)), (ip, port))

def server(ip, port):
    # Codec setup
    print("Initializing error correcting codec: Online Code")
    codec = OnlineCodec(**CODEC_PARAMS)

    # Variables
    decoder = codec.decoder()
    symbols, symbolsFeed = [], []
    decoded_blocks = {}

    status = None
    metadata = None
    corpus_size = -1
    nb_sent = -1

    # Networking setup
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.bind((ip, port))
        print("UDP socket bound to %s:%s" % (ip, port))
        # Basic signaling to start transfer. Outside of RUTP's scope. Can remove.
        pdu, addr = sock.recvfrom(NET_BUFFER_SIZE)
        if struct.unpack(PDU_BASE_FMT, pdu)[0] != 0:
            raise ValueError("Wrong protocol. Recieved PDU:", pdu)
        else: print("RUTP START")
        sock.settimeout(SOCK_TIMEOUT) # Bad if some delay…

        # Reception
        t0 = time.time()
        while not status:
            try:
                # Recieve a datagram
                pdu, addr = sock.recvfrom(NET_BUFFER_SIZE)
            except socket.timeout:
                # Signal that there is no more symbols to feed
                decoded = decoder.send((0, None))
                status = "timeout"
            else:
                if len(pdu) == PDU_BASE_SIZE:
                    # Basic signaling to stop transfer. Outside of RUTP's scope. Can remove.
                    rutp_status, nb_sent = struct.unpack(PDU_BASE_FMT, pdu)
                    if rutp_status != 1:
                        raise ValueError("Wrong protocol. Recieved PDU:", pdu)
                    decoded = decoder.send((0, None))
                    status = "failure"  # Failure unless stated otherwise (if last decoded blocks completes the corpus)
                else:
                    # Non-signaling datagram, we process it
                    rutp_status, sid, n, symbol = struct.unpack(PDU_DATA_FMT, pdu)
                    symbol_struct = (sid, n, symbol)
                    symbols.append(symbol_struct)
                    # print("Recieved data:", "sid", sid, "n", n, "S", len(symbols), "D", len(decoded_blocks))#, symbol)
                    decoded = decoder.send((1000000, [symbol_struct]))

            decoded_blocks.update(decoded)

            # Metadata found; extract corpus size
            if 0 in decoded:
                metadata = data_size, _ = struct.unpack(METADATA_FMT, unpad_block(decoded_blocks.pop(0), METADATA_SIZE))
                corpus_size = int(math.ceil(data_size / BLOCK_SIZE))

            # if last decoded blocks completes the corpus
            if len(decoded_blocks) == corpus_size:
                decoder.send((0, None))
                status = "success"

        print("Reception duration:", time.time() - t0)
        print("Status:", status)

        # Get number of bytes sent to display statistics
        # Basic signaling to stop transfer and gather stats. Outside of RUTP's scope. Can remove.
        if nb_sent == -1:
            try:
                while nb_sent == -1:
                    pdu, addr = sock.recvfrom(NET_BUFFER_SIZE)
                    # Basic signaling to stop transfer. Outside of RUTP's scope. Can remove.
                    if len(pdu) == PDU_BASE_SIZE:
                        rutp_status, nb_sent = struct.unpack(PDU_BASE_FMT, pdu)
                        if rutp_status != 1:
                            raise ValueError("Wrong protocol. Recieved PDU:", pdu)
                        print("RUTP END", nb_sent)
            except socket.timeout:
                print("RUTP END timeout!")

            if nb_sent == -1:  # Approximate default value
                print("Approximating number of blocks sent")
                nb_sent = max(symbols, key=lambda x: x[-1])[0]  # last sid
    print("Recieved blocks: {} / {} = {}%\n".format(len(symbols), nb_sent, round(len(symbols) / nb_sent * 100, 2)))

    if status == "success":
        # Transfer is completed, but some data may be corrupted
        data_size, origin_hash = metadata
        print("Metadata:", data_size, tohex(origin_hash))

        # Verify data
        reconstructed = unpad_block(b''.join((decoded_blocks[k] for k in sorted(decoded_blocks.keys()))), data_size)
        # print("Reconstructed:", reconstructed)
        hash_reconstructed = hash256(reconstructed)
        print("Hash", "OK" if hash_reconstructed == origin_hash else "ERROR!")
        print(tohex(hash_reconstructed), "(destination)")

    decoder.close()

def usage():
    return "Usage: python3.5 %s client|server" % sys.argv[0]

def process_command_line():  # use clize?
    if len(sys.argv) < 4:
        print(usage())
        sys.exit()

    ip = sys.argv[2]
    port = convert(sys.argv[3], int)

    if sys.argv[1] == "client":
        client(ip, port)
    elif sys.argv[1] == "server":
        server(ip, port)
    else:
        print(usage())

if __name__ == "__main__":
    process_command_line()
