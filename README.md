
# Package author

Nicolas Gattolin

Software licenced under the GNU General Public Licence version 3.
All rights reserved on other files.

# Synopsys

RUTP stands for Reliable Uniplex/Unidirectional Transport/Transfer Protocol.

It was mostly designed for diode links, which are direct dedicated physical links with only one permanent direction for data transmission.
RUTP is effectively a transport-layer protocol, implemented here on top of UDP to be compatible with any Internet protocol stack. 

Its characteristics are:

- integrity of each segment: using error detecting code, UDP checksum or better;
- integrity of data objects: objects are encoded with a rateless erasure correcting code (also called fountain code), expanded and diluted into a potentially infinite flow of data chunks;
- no congestion control;
- classic transport multiplexing using ports (left to UDP);

The main element of the protocol, a Forward Error Correcting Code, can also serve many purposes including distributed computing and in general limit latency effects. 

# Overview

## RUTP architecture modules

- rutp.py: client and server code comining the components
- components.py: each component of the pipeline - formatter, segmenter, error correcting codec, emitter and their inverse 
- datatypes.py: definitions of the 3 datatypes used in the protocol
- utils.py: basic integrity codes, segmenting, debugging functions

## RUTP prototype modules

- rutp_prototype.py: minimal implementation to quicly test ECC implementation
- online_code.py: 2 versions of Online Code, a rateless error correcting code

## RUTP testing/benchmarking/debugging modules

- online_code_test.py: unit tests for the CompositeOnlineCodec (1st implemented)
- online_code_graph.py: CompositeOnlineCodec implmentation using graphs to visualize progressive processing
- xor_benchmark.py: benchmark execution time of various strategies to XOR large chunks in Python
- test_files/: contains at least files of size 100Mio, 16Kio to test implementations

# Testing environment

- inbound and outbound interfaces set to MTU 9000 as recommended on dedicated links
- checksum offloading disabled on network card
(on Linux: ethtool --offload <iface> rx off tx off)
- traffic control using various combinations of rate limiting and probabilistic corruptions
(on Linux: tc qdisc add dev <iface> root netem corrupt <proba>)

# Results

Ei is input error rate, Eo is output error rate, R is ECC redundancy rate,
F is a measure of transmission efficiency computed as (P - D) / (T * C), ratio between the difference between the payload size and corrupt size, and the product of transfer time with link capacity; e.g. which proportion of the full capacity of the link was used to transport useful data to the destination.   

Typical results obtained in test environment.

Ei      | F     | R     | Eo
--------+-------+-------+-------
0%      | 82.2  | 1.13  | 0
0.5%    | 79.0  | 1.20  | 0
5%      | 76.8  | 1.25  | 0

# Dependencies

Project known to work with Debian stable versions (around 2017/09) of CPython 3.5 and the following packages.
Quickly tested the programs with CPython3.6 and NetworkX 2.3, some adaptations were necessary.
Should have used a virtualenv…

## Required

- numpy

## Optional

- networkx 1.11 or 2.X (for online_code_graph 2017 or 2020)
- matplotlib (for online_code_graph)

## Very optional

- pytest (to run online_code_test)
- memory_profiler (if want to profile memory consumption)

# Did you know?

Packet drop is in general mostly caused by congestion rather than physical errors on crowded wired networks, whereas radio network experience significant proportion of errors due to radio interferences.
