# /usr/bin/python3
# coding: utf-8

"""
Online Codes decoding algorithm analysis script
"""

import sys, re
import itertools
from statistics import mean, median, pstdev

import matplotlib.pyplot as plt, numpy as np


def iqrange(pop):
    """Returns interquartile range of a population"""
    p = sorted(list(pop))
    l = len(p)
    return p[l*3//4] - p[l//4]

def stats(pop, precision=3):
    """Displays basic stats about a population"""
    print("mean \tstddev \tmin \tmedian \tmax \tiqr \tN")
    print('\t'.join(str(round(f(pop), precision)) for f in (mean, pstdev, min, median, max, iqrange)) + '\t' + str(len(pop)))

def display(*series, **kwargs):
    """Diaplays labeled series in a graph, with optional title and scales"""
    if "title" in kwargs:
        plt.title(kwargs.pop("title"))
    if "xscale" in kwargs:
        plt.xscale(kwargs.pop("xscale"))
    if "yscale" in kwargs:
        plt.yscale(kwargs.pop("yscale"))
    for pop, label in series:
        plt.plot(pop, label=label, **kwargs)
    plt.legend()
    plt.show()

def redundancy(lines):
    """Takes as input online code test output"""
    r = [float(l.split()[-1]) for l in lines if l.startswith("Redundancy")]
    t = [float(l.split()[-1]) for l in lines if l.startswith("Decoding duration")]
    first = next(l for l in lines if " n:" in l)
    second = next(l for l in lines if " e:" in l)
    print(first + second, end='')
    print("Stats for redundancy")
    stats(r, 2)
    print("Stats for decoding time")
    stats(t, 3)

def decoding_sequence(lines):
    """Takes as input online code debug output such as:
l: 104857600 n: 25600 bs: 4096 q: 3 e: 0.01 F: 2114 P1: 0.0062485 s: [2561464167, 3168525865] a: 0
c1299s7c2164s10c2881s14c3531s19c4144s31c4686s43c5200s56c5683s62c6152s64c6601s68c7028s84
where 'c' and 's' are number of consecutive iterations for resp. check or source blocks
    """
    dseq = re.split(r"[sc]", lines[1].strip())[1:]  # empty val
    dseq = list(map(int, dseq))
    C, S = np.array(dseq[::2]), np.array(dseq[1::2])
    Sacc, Cacc = np.array(list(itertools.accumulate(S))), np.array(list(itertools.accumulate(C)))

    title = "Input CB and recovered SB iterations during decoding process"
    display((S, "source"), (C, "check"), title=title)
    display((Sacc, "source"), (Cacc, "check"), title=title)

def decoding_time(lines):
    """Takes as input online code test output
l: 104857600 n: 25600 bs: 4096 q: 3 e: 0.01 F: 2114 P1: 0.0062485 s: [2561464167, 3168525865] a: 0
c1299s7 c2164s10 c2881s14 c3531s19 c4144s31 c4686s43 c5200s56 c5683s62 c6152s64 c6601s68 c7028s84
where 'c' and 's' are number of iterations for resp. check or source blocks in a fixed time lapse
    """
    #dtime = [[int(v.strip("cs")) for v in re.split(r"[cs ]", l.strip())] for l in lines[1]]
    Cacc, Sacc = zip(*(map(int, re.split(r"[cs]", v)[1:]) for v in lines[1].strip().split()))

    title = "Input CB and recovered SB with respect to time during decoding process"
    display((Sacc, "source"), (Cacc, "check"), title=title)

if __name__ == "__main__":
    # forme de la courbe: CB converge vers (1+e)n, SB en symétrique

    usage = """Usage: python3 %s input_filename stats|dseq|dtime
`stats` displays stats about redundancy and decoding time in tests results
`dseq` displays a graph of decoding performance iteration by iteration
`dtime` displays a graph of decoding performance over time""" % sys.argv[0]
    if len(sys.argv) < 3:
        print(usage)
        sys.exit()

    with open(sys.argv[1]) as f:
        lines = f.readlines()

    if sys.argv[2] == "stats":
        redundancy(lines)
    elif sys.argv[2] == "dseq":
        decoding_sequence(lines)
    elif sys.argv[2] == "dtime":
        decoding_time(lines)
    else:
        print(usage)

