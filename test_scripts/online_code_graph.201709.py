# /usr/bin/python3
# coding: utf-8

"""
Online Codes implementation using a specific graph datatype

block_size = (parameter)
n          = data_size / block_size
q          = 3 (parameter)
e          = 0.01 (parameter)

number of auxillary blocks: .55 * q * e * n

Outer encoding:
q auxillary blocks XOR of attached source blocks

Inner encoding:
Random distribution:
F = ceil . ln(e**2 / 4) / ln(1 - e / 2)
P1 = 1 - (1 + 1 / F) / (1 + e)
For i from 2 to F
    Pi = (1 - P1) * F / ((F - 1) * i * (i - 1))
"""

import sys, time, math, random, bisect
from math import ceil, log
import collections, itertools
from functools import reduce, partial
from typing import Iterable, Sequence, MutableSequence, List, Mapping, MutableMapping, Dict, Tuple, Generator, Any, Optional, NewType, cast

import numpy as np # 450ms startup
from numpy import fromstring, bitwise_xor, uint8
import matplotlib.pyplot as plt
import networkx as nx
from networkx.algorithms import bipartite

from utils import pad_block, unpad_block

SBID = NewType("SBID", int)
CBID = NewType("CBID", int)
SourceBlock = NewType("SourceBlock", bytes)
CheckBlock = NewType("CheckBlock", bytes)


# Utilities for codec

_debug = False
def debug(*args, **kwargs):
    if _debug: print(*args, **kwargs)

def consumer(func):
    def start(*args, **kwargs):
        c = func(*args, **kwargs)
        next(c)
        return c
    return start

def updateTimings(tl, tn, ncb, nsb):
    tn = time.time()
    if tn > tl + .5:
        print("c%ss%s" % (ncb, nsb), end=' ', flush=True)
        tl = tn
    return tl, tn

def sample(pop: Sequence[Any], size: Optional[int], replace=True, pdf: Optional[Sequence[float]] = None) -> \
Sequence[Any]:
    if not replace and size and size > len(pop): return pop
    return np.random.choice(pop, size, replace=replace, p=pdf)

def sample_unique(pop:Sequence[Any], size:int, prng=random) -> Sequence[Any]:
    if size > len(pop): return pop
    #result = random.sample(pop, size)  # Incompatible with np.ndarray sadly...
    # Adapted from CPython random.sample
    randbelow = prng.randrange
    n = len(pop)
    result = [None] * size
    setsize = 21  # size of a small set minus size of an empty list
    if size > 5:
        setsize += 4 ** ceil(log(size * 3, 4))  # table size for big sets
    if n <= setsize:
        # An n-length list is smaller than a k-length set
        pool = list(pop)
        for i in range(size):  # invariant:  non-selected at [0,n-i)
            j = randbelow(n - i)
            result[i] = pool[j]
            pool[j] = pool[n - i - 1]  # move non-selected item into vacancy
    else:
        selected = set()
        selected_add = selected.add
        for i in range(size):
            j = randbelow(n)
            while j in selected:
                j = randbelow(n)
            selected_add(j)
            result[i] = pop[j]
    # print("sample_unique:", result)
    return result

def choose_degree(cdf:Sequence[float], prng=random, _search_func=bisect.bisect_left) -> int:
    rand = prng.random()
    idx = _search_func(cdf, rand) + 1
    # print("choose_degree:", idx)
    return idx

def xor_blocks_np(blocks:Iterable[np.ndarray]) -> np.ndarray:  # Functional style: a bit faster
    return reduce(bitwise_xor, blocks)

def xor_blocks(blocks:Iterable[bytes]) -> bytes:  # Functional style: a bit faster
    return reduce(bitwise_xor, (fromstring(blk, dtype=uint8) for blk in blocks)).tostring()

def conv(id, type_, cast=False):
    if cast: id = id[1:]
    if type_ == 's': return "s%s" % id
    if type_ == 'c': return "c%s" % id
    if type_ == 'a':
        return "a%s" % id
    else:
        raise ValueError("conv", type_, id)

def ipartition(items, predicate=bool):
    a, b = itertools.tee((predicate(item), item) for item in items)
    return ((item for pred, item in a if pred),
            (item for pred, item in b if not pred))

# Graph stuff

def draw_graph(graph, highlight_adj=None):
    # X, Y = bipartite.sets(graph)
    # pos = dict(itertools.chain(((n, (1, i)) for i, n in enumerate(X)), ((n, (2, i)) for i, n in enumerate(Y))))
    # pos = nx.nx_pydot.graphviz_layout(graph, prog="neato")  # neato, sfdp
    pos = nx.spring_layout(graph)  # circular/shell
    nx.draw_networkx_nodes(graph, pos=pos, nodelist=[v for v in graph.adj if v[0] == 's'], node_color='g', node_size=150, alpha=.5)
    nx.draw_networkx_nodes(graph, pos=pos, nodelist=[v for v in graph.adj if v[0] == 'c'], node_color='r', node_size=150, alpha=.5)
    nx.draw_networkx_nodes(graph, pos=pos, nodelist=[v for v in graph.adj if v[0] == 'a'], node_color='y', node_size=150, alpha=.5)
    if highlight_adj:
        # highlighted_edges, other_edges = map(list, ipartition(graph.edges(), lambda edge: edge[0] in highlight_adj))
        highlighted_edges, other_edges = map(list, ipartition(graph.edges(),
                                                              lambda edge: any(node in highlight_adj for node in edge)))
        # print(list(highlighted_edges), list(other_edges))
        nx.draw_networkx_edges(graph, pos=pos, edgelist=highlighted_edges, edge_color='b', alpha=.5, arrows=False)
        nx.draw_networkx_edges(graph, pos=pos, edgelist=other_edges, alpha=.5, arrows=False)
    else:
        nx.draw_networkx_edges(graph, pos=pos, alpha=.5, arrows=False)
    nx.draw_networkx_labels(graph, pos=pos, font_size=9, font_weight="bold")
    # nx.draw_shell(graph, node_size=150, alpha=.5, arrows=False, with_labels=True, font_size=9, font_weight="bold")
    plt.axis('off')
    plt.axis('tight')
    input()
    #plt.pause(.2)
    plt.clf()

class LightGraph(nx.Graph):
    edge_attrs = {}
    edge_attr_dict_factory = lambda self: self.edge_attrs


class CompositeOnlineCodec:

    def __init__(self, n:int, q:int=3, e:float=.01, f:int=0, aseed:int=1, dseed:int=1) -> None:
        """
        N could be optional to the codec, but several properties rely on it to improve efficiency
        Rate increases with high block degrees predominancy
        Algorithm works faster with fewer blocks (n is way more important than block size)
        """
        assert q > 0 and e > 0
        self.n = n  # number of blocks
        self.q = q  # quality
        self.e = e  # efficiency (suboptimality)

        self.f = f or int(log(e ** 2 / 4) / log(1 - e / 2))
        self.compute_distribution()
        print("q:", self.q, "e:", self.e, "F:", self.f, "P1:", self.p[0])
        self.adjacency_prng = random.Random()
        self.degree_prng = random.Random()
        self.seed(aseed, dseed)

        # Precalculate to speed up algorithm
        self.degrees = np.arange(1, self.f + 1)
        self.indices_aux = range(n)
        self.indices_src = range(n + self.auxillary_blocks_number())

        # For debugging purpose
        self.degree_history = []  # type: List[int]

    def compute_distribution(self):
        # Compute online code soliton distribution
        f, n = self.f, self.n
        p = np.empty(f, dtype=np.float32)
        p1 = n**-.5  # 1 - (1 + 1 / f) / (1 + self.e)
        p[0] = p1
        for i in range(1, f):
            p[i] = (1 - p1) * f / ((f - 1) * (i + 1) * (i))
        # print("Distribution:", p)
        # with open("online_distribution_%s_%s" % (e, n), "w") as file: file.write('\n'.join(str(pp) for pp in p))
        self.p = p
        self.cdf = list(itertools.accumulate(p))
        assert math.isclose(sum(p), self.cdf[-1], abs_tol=1e-5) and math.isclose(self.cdf[-1], 1., abs_tol=1e-5)

    def seed(self, aseed:int=1, dseed:int=1):
        self.adjacency_prng_seed = aseed or np.random.randint(0, 2 ** 32)
        self.degree_prng_seed = dseed or np.random.randint(0, 2 ** 32)
        print("Seeds:", [self.adjacency_prng_seed, self.degree_prng_seed])

    def auxillary_blocks_number(self) -> int:
        return 0#int(ceil(.55 * self.q * self.e * self.n))

    def min_needed_output_blocks(self) -> int:
        return int(ceil((1 + self.e) * (self.n + self.auxillary_blocks_number())))

    def gen_cbid(self) -> Generator[CBID, None, None]:
        yield from map(CBID, itertools.count())

    sample = staticmethod(sample)
    sample_unique = staticmethod(sample_unique)
    choose_degree = staticmethod(choose_degree)

    def degree_from(self, seed:int):
        # Cannot precompute degrees because must seed with CBID
        random.seed(self.degree_prng_seed + seed)
        # 1 choice in [1, F] given probability density function P
        d = self.choose_degree(cdf=self.cdf)
        self.degree_history.append(d)
        return d

    def encoder(self, source_blocks:MutableSequence[SourceBlock]) -> Generator[Tuple[CBID, CheckBlock], None, None]:
        """Generator encoder for online codes.
        It takes source blocks as parameter and produces an infinite iterator of check blocks.
        It mutates the sequence of source blocks.
        """
        q, n_aux = self.q, self.auxillary_blocks_number()
        aprng, dprng = self.adjacency_prng, self.degree_prng
        print("N aux:", n_aux)

        # Outer encoding

        aprng.seed(self.adjacency_prng_seed)
        for abid in range(n_aux):
            src_indices = self.sample_unique(self.indices_aux, q, prng=aprng)
            aux_block = xor_blocks([source_blocks[src_idx] for src_idx in src_indices])
            #debug("Auxillary block:", abid, list(src_indices), [b.tostring() for b in source_blocks[n:]])
            source_blocks.append(SourceBlock(aux_block))
        #debug("Auxillary blocks:", n_aux, [b.tostring() for b in source_blocks[n:]])
        debug()

        cbid_gen = self.gen_cbid()
        while True:
            # Inner encoding
            cbid = next(cbid_gen)
            #debug("CBID", cbid, end=' ')

            dprng.seed(self.degree_prng_seed + cbid)
            d = self.choose_degree(cdf=self.cdf, prng=dprng)  # type: int
            self.degree_history.append(d)

            aprng.seed(self.adjacency_prng_seed + cbid)
            src_indices = self.sample_unique(self.indices_src, d, prng=aprng)

            check_block = xor_blocks([source_blocks[idx] for idx in src_indices])
            # debug(cbid, "adjacency", d, list(src_indices))
            # debug(" ", check_block)
            yield (cbid, CheckBlock(check_block))

    @consumer
    def decoder(self) -> Generator[Dict[SBID, SourceBlock], MutableMapping[CBID, CheckBlock], None]:
        """Generator decoder for online codes.
        It takes input for each step with the send(object) method of generator objects,
        and produce decoded blocks or None if there is not enough data to decode blocks.
        The coroutine decorator applied on this function bootstraps the generator on creation.

        This version uses a single data structure for graph representation and does not work with sampling with replacement.
        """
        # plt.ion()
        # fig = plt.figure(figsize=(8, 8))  # Wider window, F/Ctrl-F fullscreen
        # fig.subplots_adjust(left=0, right=1, bottom=0, top=1)

        q = self.q

        graph = LightGraph()
        recovered_blocks = {}  # type: Dict[str, np.ndarray]
        check_blocks = {}  # type: Dict[str, np.ndarray]
        one_degree = []
        n_sb_recovered = 0
        tl = tn = time.time()
        known_other_cb_filter = lambda cbid: cbid in check_blocks and cbid != current_cbid

        np.random.seed(self.adjacency_prng_seed)
        for i in range(self.auxillary_blocks_number()):
            adj = [conv(sbid, 's') for sbid in self.sample(self.indices_aux, q, replace=False)]
            aux_cbid = conv(self.n + i, 'a')
            graph.add_edges_from([(sbid_, aux_cbid) for sbid_ in adj])
        # debug("Initial adjacency map:")
        # self.printAdjacency(graph)
        # draw_graph(graph)

        while n_sb_recovered != self.n:
            # Get next free check block, if there is none, yield and wait for more
            if not one_degree:
                debug()
                it = 0
                more_check_blocks = yield None  # Means "I cannot fully decode yet! Gimme more!"
                if more_check_blocks is None: break
                more_check_blocks = {conv(cbid, 'c'): fromstring(cb, dtype=uint8) for cbid, cb in more_check_blocks.items()}
                # debug("Incoming check blocks:", ' '.join(more_check_blocks))
                check_blocks.update(more_check_blocks)

                # Update adjacency map
                for cbid in more_check_blocks.keys():
                    d = self.degree_from(int(cbid[1:]))  # type: int
                    adj = [conv(sbid, 's') for sbid in self.sample(self.indices_src, d, replace=False)]  # type: List[SBID]
                    # debug("Input Adjacency before reduction:", ' '.join(str(v) for v in adj))
                    for sbid in adj[:]:
                        if sbid in recovered_blocks:
                            # debug("Removing already recovered", sbid)
                            adj.remove(sbid)
                            check_blocks[cbid] = xor_blocks_np([check_blocks[cbid], recovered_blocks[sbid]])
                            # debug("Reduced:", cbid, check_blocks[cbid].tostring())
                    graph.add_edges_from([(cbid, sbid_) for sbid_ in adj])
                    # debug("Input Adjacency after reduction:", ' '.join(str(v) for v in adj))
                    if len(adj) == 1: one_degree.append(cbid)  #; debug("Input Found one_degree")
                    # if len(check_blocks) == self.min_needed_output_blocks():
                    #     debug("\n----------MIN---------"); self.printAdjacency(graph); debug("----------MIN---------\n")
                it += 1
                #tl, tn = updateTimings(tl, tn, sum(1 for node in graph.nodes_iter() if node[0] == 'c'), n_sb_recovered)
            #print("c" + str(it), end='', flush=True)
            it = 0
            while one_degree:
                # debug()
                # debug("C", len(check_blocks), sorted(list(check_blocks)))
                # debug("S", len(recovered_blocks), sorted(list(recovered_blocks)))
                # debug("O", len(one_degree), sorted(one_degree))

                current_cbid = one_degree.pop()
                current_sbid, = graph.adj[current_cbid]

                # debug("To recover (CBID, SBID):", current_cbid, current_sbid)
                # debug("CBIDs:", ' '.join(str(v) for v in list(filter(known_other_cb_filter, graph.adj[current_sbid]))))
                # draw_graph(graph, [current_cbid, current_sbid])
                graph.remove_edge(current_cbid, current_sbid)
                #debug("Adjacency map before reduction:")
                #self.printAdjacency(graph)

                # This is inner + outer decoding, because auxillary blocks become check blocks when found
                recovered_block = check_blocks.pop(current_cbid)
                # debug("Recovered:", current_sbid, recovered_block.tostring())
                # Get all CB that are known and adjacent to SB
                for cbid in filter(known_other_cb_filter, list(graph.adj[current_sbid])):  # Reduce adjacency
                    check_blocks[cbid] = xor_blocks_np([check_blocks[cbid], recovered_block])
                    # debug("Reduced:", cbid, check_blocks[cbid].tostring())
                    graph.remove_edge(cbid, current_sbid)
                    cb_len = len(graph.adj[cbid])
                    if cb_len == 1:
                        one_degree.append(cbid)  #; debug("Found one_degree")
                    elif cb_len == 0:
                        one_degree.remove(cbid)  #; debug("Removed one_degree")
                        del check_blocks[cbid]

                # Store depending on whether block must be processed further
                # if current_sbid in recovered_blocks:
                #     print("/!\ Already recovered:", current_sbid)
                if int(current_sbid[1:]) < self.n:
                    recovered_blocks[current_sbid] = SourceBlock(recovered_block)
                    n_sb_recovered += 1
                    #debug("Recovered source block:", current_sbid, "n:", n_sb_recovered)
                else:
                    cbid_aux = conv(current_sbid, 'a', cast=True)
                    # debug("\nFound auxillary block:", current_sbid, "->", cbid_aux)
                    # adj = graph.adj[cbid_aux]
                    # debug("Aux Adjacency before reduction:", adj)
                    # for sbid in list(adj):
                    #     if sbid in recovered_blocks:
                    #         graph.remove_edge(cbid_aux, sbid)
                    #         recovered_block = xor_blocks_np([recovered_block, recovered_blocks[sbid]])
                    #         debug("Reduced:", cbid_aux, recovered_block.tostring())
                    # if len(adj) == 1: one_degree.append(cbid_aux)  ; debug("Aux Found one_degree")
                    # debug("Aux Adjacency after reduction:", graph.adj[cbid_aux])
                    check_blocks[cbid_aux] = CheckBlock(recovered_block)
                    recovered_blocks[cbid_aux] = SourceBlock(recovered_block)  # Only to determine if known
                #debug("Recovered blocks:", len(recovered_blocks.keys()), list(recovered_blocks.keys()))
                it += 1
            tl, tn = updateTimings(tl, tn, sum(1 for node in graph.nodes_iter() if node[0] == 'c'), n_sb_recovered)
            #print("s" + str(it), end='', flush=True)
        #print("c%ss%s" % (len(adjacencyCB), n_sb_recovered))
        #input()
        n_cb = sum(1 for node in graph.nodes_iter() if node[0] == 'c')
        n_sb = sum(1 for node in graph.nodes_iter() if node[0] == 's')
        n_ab = sum(1 for node in graph.nodes_iter() if node[0] == 'a')
        print(n_cb, n_sb, n_ab)
        print("Redundancy:", n_cb, '/', n_sb_recovered, '=', n_cb / n_sb_recovered)
        print("Rate:", n_sb_recovered, '/', n_cb, '=', n_sb_recovered / n_cb)
        yield {int(sbid[1:]): sb.tostring() for sbid, sb in recovered_blocks.items() if int(sbid[1:]) < self.n}

    def printAdjacency(self, graph:nx.Graph):
        #debug(dict.__repr__(adj))
        debug("{%s}" % ' '.join("[%s:%s]" % (k, ' '.join(v)) for k, v in graph.adj.items()))

def testEncode(codec:CompositeOnlineCodec, blocks:MutableSequence[SourceBlock]) -> Dict[CBID, CheckBlock]:
    symbols = {}
    encoder = codec.encoder(blocks)
    enough = codec.min_needed_output_blocks()
    for cbid, sym in itertools.islice(encoder, int(enough * 2)):
        symbols[cbid] = sym
    del encoder
    return symbols

def testDecode(codec:CompositeOnlineCodec, symbols:MutableMapping[CBID, CheckBlock]) -> Dict[SBID,SourceBlock]:
    decoder = codec.decoder()
    decoded = None  # type: Optional[Dict[SBID, SourceBlock]]
    while not decoded:
        k = min(len(symbols), 1)
        symbolsFeed = [symbols.popitem() for _ in range(k)]  # type: Tuple[CBID, CheckBlock]
        if not symbolsFeed: decoded = decoder.send(None)
        else: decoded = decoder.send(dict(symbolsFeed))
    del decoder
    return decoded

def test():
    # TODO (done): profile and optimize random choice
    # TODO (done): change distribution computation, data structure (float array)
    # TODO (done): test different block segmentations (-> n bigger impact than bs)
    # TODO (done): test different q, epsilon (must vary according to n)
    # TODO: do complexity analysis of algorithm (number of calls, iterations)
    # TODO (done): are the auxillary blocks used efficiently? no
    # TODO: compare with other implementations

    import cProfile
    from copy import deepcopy
    import gc

    with open("test_files/100Mio", "rb") as file:
        data = file.read()

    data_size = len(data)
    block_size = 8192
    n = int(ceil(data_size / block_size))
    print("Message length:", data_size, "n:", n, "bs:", block_size)
    blocks = [SourceBlock(data[i * block_size: i * block_size + block_size]) for i in range(n)]
    blocks[-1] = SourceBlock(pad_block(blocks[-1], block_size))

    codec = CompositeOnlineCodec(n, q=3, e=.01, aseed=0, dseed=0)#, f=200)
    print("Minimum check blocks needed:", codec.min_needed_output_blocks())

    for i in range(5):
        print()
        # codec.seed(2561464167, 3168525865)
        codec.seed(0, 0)

        t0 = time.time()
        symbols = testEncode(codec, deepcopy(blocks))
        print("Encoding duration:", time.time() - t0)
        #cProfile.run("testEncode(codec, blocks)", sort="time")

        c = collections.Counter(codec.degree_history)
        print("\nDegrees:", (1, c[1]), c.most_common(6), "…")

        print()

        # loss_rate = .05#round(np.random.random() * .5, 2)
        # lost_cbid_list = np.random.choice(list(symbols.keys()), int(loss_rate * len(symbols)), replace=False)
        # print("Lost {}% symbols: {} -> {}\n".format(loss_rate*100, len(symbols), (len(symbols) - len(lost_cbid_list))))
        # for lost_cbid in lost_cbid_list:
        #     del symbols[lost_cbid]

        t0 = time.time()
        decoded = testDecode(codec, symbols)
        print("Decoding duration:", time.time() - t0)
        #cProfile.run("testDecode(codec, symbols)", sort="time")
        #debug("\nDecoded:", decoded)

        if decoded:
            reconstructed = unpad_block(b''.join((decoded[k] for k in sorted(decoded.keys()))), data_size)
            debug('\n', reconstructed, '\n')
            print("Is complete:", data == reconstructed)
            if data != reconstructed:
                corrupt = {}
                debug("Lengths:", len(decoded), len(blocks))
                for i, k in enumerate(decoded):
                    if decoded[k] != blocks[i]:
                        corrupt[k] = decoded[k]
                #debug("Corrupt blocks:", list(corrupt))
            del reconstructed
        else:
            print("Decoding failed")
        del symbols
        del decoded
        debug("GC:", gc.collect())

    del data
    del blocks
    del codec

if __name__ == "__main__":
    test()