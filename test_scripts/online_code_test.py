# /usr/bin/python3
# coding: utf-8

"""
Online Codes tests
"""

import math, itertools
from copy import deepcopy

import pytest
from unittest.mock import MagicMock

import online_code
from utils import slice_dict



class TestXor:

    @pytest.fixture()
    def two_blocks(self):
        return [b"\0" * 8, b"\xFF" * 8]

    @pytest.fixture()
    def n_blocks(self):
        return [i.to_bytes(1, "little") * 8 for i in range(10)]

    @pytest.fixture()
    def same_blocks(self):
        return [b"\xAA" * 8, b"\xAA" * 8]

    @pytest.fixture()
    def big_blocks(self):
        return [b"\0" * 2 ** 12, b"\1" * 2 ** 12, b"\xFF" * 2 ** 12]

    def test_xor_two_blocks(self, two_blocks):
        assert online_code.xor_blocks(two_blocks) == b"\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF"

    def test_xor_n_blocks(self, n_blocks):
        assert online_code.xor_blocks(n_blocks) == b"\x01\x01\x01\x01\x01\x01\x01\x01"

    def test_xor_same(self, same_blocks):
        assert online_code.xor_blocks(same_blocks) == b"\0\0\0\0\0\0\0\0"

    def test_xor_big_blocks(self, big_blocks):
        assert online_code.xor_blocks(big_blocks) == b"\xFE" * 2**12


class TestSample:
    """
    sample(pop:Sequence[Any], size:int, replace=True, pdf=None) -> Sequence[Any]
    properties:
    - pop:
        - None: error
    - size:
        - in ]0, len(pop)]: sample randomly from pop
        - size > len(pop) and replace == False: return pop
        - else: error
    - replace:
        - True: sample independently
        - False: sample uniquely
    - pdf:
        - None: sample uniformly
        - else:
            - sample using probability distribution
            - len(pdf) == len(pop)
            - each value in [0, 1]
            - sum of values == 1
            - else: error
    - return:
        - ret in pop
        - replace is False: ret has unique elements
        - pdf not None: not any element of ret had probability 0
    """
    # TODO: mock PRNG!

    @pytest.fixture()
    def pop_1(self):
        return [-2, -1, 0, 1, 2]

    @pytest.fixture()
    def pop_empty(self):
        return []

    @pytest.fixture()
    def pop_one(self):
        return [0]

    @pytest.fixture()
    def pop_duplicates(self):
        return [0, 1, 0, 1, 1]

    @pytest.fixture()
    def pop_uniform(self):
        return [1, 1, 1, 1, 1]

    @pytest.fixture()
    def pdf_1(self):
        return [.1, .5, .2, .1, .1]

    @pytest.fixture()
    def pdf_empty(self):
        return []

    @pytest.fixture()
    def pdf_one(self):
        return [1.]

    @pytest.fixture()
    def pdf_uniform(self):
        return [.2, .2, .2, .2, .2]

    @pytest.fixture()
    def pdf_length_mismatch(self):
        return [.2, .5, .3]

    @pytest.fixture(params=[-1, 2])
    def pdf_val_invalid(self, request):
        return [.1, request.param, .1, .1, .1]

    @pytest.fixture(params=[.1, .8])
    def pdf_sum_invalid(self, request):
        return [.1, request.param, .1, .1, .1]

    @pytest.fixture()
    def pdf_zeros(self):
        return [.5, 0, .5, 0, 0]


    def test_sample_basic(self, pop_1, pdf_1):
        online_code.sample(pop_1, 1, True, None)
        online_code.sample(range(5), 1, True, pdf_1)
        online_code.sample(pop_1, 1, False, pdf_1)
        online_code.sample(pop_1, 1, True, pdf_1)

    def test_sample_size(self, pop_1, pdf_1):
        assert len(online_code.sample(pop_1, 1, True, pdf_1)) == 1
        assert len(online_code.sample(pop_1, 5, True, pdf_1)) == 5
        assert len(online_code.sample(pop_1, 1, False, pdf_1)) == 1
        assert len(online_code.sample(pop_1, 5, False, pdf_1)) == 5

    def test_sample_no_replace_size_greater(self, pop_1, pdf_1):
        assert set(online_code.sample(pop_1, len(pop_1) + 1, False, None)) == set(pop_1)
        assert set(online_code.sample(pop_1, len(pop_1) + 1, False, pdf_1)) == set(pop_1)

    def test_sample_replace_size_greater(self, pop_1, pdf_1):
        ret = online_code.sample(pop_1, len(pop_1) + 1, True, None)
        assert set(ret).issubset(set(pop_1)) # ret without duplicates smaller than pop
        ret = online_code.sample(pop_1, len(pop_1) + 1, True, pdf_1)
        assert set(ret).issubset(set(pop_1)) # ret without duplicates equals pop

    def test_sample_contains(self, pop_1, pdf_1):
        assert online_code.sample(pop_1, 1, True, None)[0] in pop_1
        assert online_code.sample(pop_1, 1, False, None)[0] in pop_1
        assert online_code.sample(pop_1, 1, True, pdf_1)[0] in pop_1
        assert online_code.sample(pop_1, 1, False, pdf_1)[0] in pop_1

    def test_sample_empty(self, pop_empty, pdf_empty):
        with pytest.raises(ValueError):
            online_code.sample(pop_empty, 1, True, None)
            online_code.sample(pop_empty, 1, False, None)
            online_code.sample(pop_empty, 1, True, pdf_empty)
            online_code.sample(pop_empty, 1, False, pdf_empty)

    def test_sample_pop_invalid(self):
        with pytest.raises(ValueError):
            online_code.sample(None, 1, True, None)

    def test_sample_size_invalid(self, pop_1):
        with pytest.raises(ValueError):
            online_code.sample(pop_1, -1, True, None)

    def test_sample_pdf_val_invalid(self, pop_1, pdf_val_invalid):
        with pytest.raises(ValueError):
            online_code.sample(pop_1, 1, True, pdf_val_invalid)

    def test_sample_pdf_sum_invalid(self, pop_1, pdf_sum_invalid):
        with pytest.raises(ValueError):
            online_code.sample(pop_1, 1, True, pdf_sum_invalid)

    def test_sample_pdf_length_mismatch(self, pop_1, pdf_length_mismatch):
        with pytest.raises(ValueError):
            online_code.sample(pop_1, 1, True, pdf_length_mismatch)

    def test_sample_pop_one(self, pop_one, pdf_one):
        s = 2
        expected1 = [pop_one[0]]
        expected2 = [pop_one[0]] * s
        assert list(online_code.sample(pop_one, s, False, None)) == expected1
        assert list(online_code.sample(pop_one, s, False, pdf_one)) == expected1
        assert list(online_code.sample(pop_one, s, True, None)) == expected2
        assert list(online_code.sample(pop_one, s, True, pdf_one)) == expected2

    def test_sample_pop_uniform(self, pop_uniform, pdf_1):
        s = 2
        expected = [pop_uniform[0]] * s
        assert list(online_code.sample(pop_uniform, s, False, None)) == expected
        assert list(online_code.sample(pop_uniform, s, False, pdf_1)) == expected
        assert list(online_code.sample(pop_uniform, s, True, None)) == expected
        assert list(online_code.sample(pop_uniform, s, True, pdf_1)) == expected

    def test_sample_pdf_uniform(self, pop_uniform, pdf_uniform):
        s = 2
        assert list(online_code.sample(pop_uniform, s, False, pdf_uniform)) \
            == list(online_code.sample(pop_uniform, s, False, pdf_uniform))
        assert list(online_code.sample(pop_uniform, s, True, pdf_uniform)) \
            == list(online_code.sample(pop_uniform, s, True, pdf_uniform))

    def test_sample_pdf_proba0(self, pop_1, pdf_zeros):
        assert pdf_zeros.count(0) == 3
        assert len(online_code.sample(pop_1, 2, False, pdf_zeros)) == 2
        assert len(online_code.sample(pop_1, 2, True, pdf_zeros)) <= 2
        with pytest.raises(ValueError):
            online_code.sample(pop_1, 5, False, pdf_zeros)  # numpy rejects size > number of non-zero in pop


class TestCodecInit:

    def test_codec_init(self):
        codec = online_code.CompositeOnlineCodec(3, 3, .01, aseed=0, dseed=0)
        assert codec.n > 0
        assert codec.q > 0
        assert 0 < codec.e < 1
        assert codec.f > 0
        assert len(codec.p) > 0
        assert 0 <= codec.adjacency_prng_seed <= 2**32 - 1  # numpy.random.seed limitation
        assert 0 <= codec.degree_prng_seed <= 2**32 - 1
        #assert codec.auxillary_blocks_number() == 1
        assert codec.min_needed_output_blocks() == 4 + codec.auxillary_blocks_number()


class TestCodecDegree:

    @pytest.fixture(params=[(3, 3, .01, 0), (3, 5, .05, 2), (1024, 3, .01, 1)])
    def codec(self, request):
        n, q, e, dseed = request.param
        codec = online_code.CompositeOnlineCodec(n, q, e, dseed=dseed)
        return codec

    def test_degree(self, codec):
        P = codec.p
        F = codec.f
        # Test distribution
        assert len(P) == F
        assert all(0 <= p <= 1 for p in P)
        assert P[0] > 0
        for i in range(2, len(P)):  # Decreasing starting with second item
            assert P[i] <= P[i-1]
        # Test some degrees
        assert 1 <= codec.degree_from(0) <= F
        assert 1 <= codec.degree_from(1000) <= F


class TestCodecProcess:

    @pytest.fixture()
    def codec(self):
        return online_code.CompositeOnlineCodec(3, 3, .01, aseed=1, dseed=1)

    @pytest.fixture()
    def small_blocks(self):
        return [b"\0" * 8, b"\1" * 8, b"\xFF" * 8]

    @pytest.fixture()
    def medium_blocks(self):
        return [b"\0" * 2 ** 12, b"\1" * 2 ** 12, b"\xFF" * 2 ** 12]

    @pytest.fixture()
    def small_symbols(self):
        return  {0: b'\xFE'*8, 1: b'\1'*8, 2: b'\1'*8, 3: b'\0'*8,
                 4: b'\xFF'*8, 5: b'\0'*8, 6: b'\0'*8, 7: b'\0'*8}

    def test_codec_basic(self, codec, small_blocks):
        assert len(small_blocks) == 3

        enough = codec.min_needed_output_blocks()
        symbols = dict(itertools.islice(codec.encoder(deepcopy(small_blocks)), enough*2+2))
        assert enough <= len(symbols) <= enough * 2+2

        decoder = codec.decoder()
        ret = decoder.send(symbols)
        # To avoid infinite iteration and verify if too few or too many
        ret = dict(list(itertools.islice(filter(None, ret.items()), 3+1)))
        assert list(ret.values()) == small_blocks

    def test_encode(self, codec, small_blocks):
        if codec.auxillary_blocks_number() == 1:  # and also if non-independent sampling...
            vector = [
               # d, indices,       expected
                (2, [2, 1],        (0, b'\xFE'*8) ),
                (2, [3, 2],        (1, b'\1'*8)   ),
                (2, [3, 2],        (2, b'\1'*8)   ),
                (26, [0, 1, 2, 3], (3, b'\0'*8)   ),
                (2, [2, 0],        (4, b'\xFF'*8) ),
                (8, [0, 1, 2, 3],  (5, b'\0'*8)   ),
                (1, [0],           (6, b'\0'*8)   ),
                (7, [0, 1, 2, 3],  (7, b'\0'*8)   ),
            ]
        else:  # no aux blocks and independent sampling
            vector = [
                # d, indices,       expected
                (2, [0, 0], (0, b'\0' * 8)),
                (2, [1, 0], (1, b'\1' * 8)),
                (3, [1, 0, 0], (2, b'\1' * 8)),
                (30, [1, 1, 0, 0, 2, 1, 2, 0, 1, 2, 1, 0, 0, 2, 0, 2, 2, 2, 0, 0, 1, 1, 1, 2, 1, 2, 0, 2, 2, 1], (3, b'\xFF' * 8)),
                (2, [1, 2], (4, b'\xFE' * 8)),
                (10, [0, 2, 1, 2, 0, 2, 1, 1, 1, 1], (5, b'\xFE' * 8)),
            ]

        codec.sample = MagicMock(return_value=[0, 1, 2])
        encoder = codec.encoder(small_blocks)

        for d, indices, expected in vector:
            codec.degree_from = MagicMock(return_value=d)
            codec.sample = MagicMock(return_value=indices)
            ret = encoder.send(None)
            assert ret == expected

    def test_decode(self, codec, small_symbols, small_blocks):
        vector = [
           # d, indices,       symbol,                                 expected
            (2, [2, 1],        list(slice_dict(small_symbols, 0, 1)),  None),
            (2, [3, 2],        list(slice_dict(small_symbols, 1, 2)),  None),
            (2, [3, 2],        list(slice_dict(small_symbols, 2, 3)),  None),
            (26, [0, 1, 2, 3], list(slice_dict(small_symbols, 3, 4)),  None),
            (2, [2, 0],        list(slice_dict(small_symbols, 4, 5)),  None),
            (8, [0, 1, 2, 3],  list(slice_dict(small_symbols, 5, 6)),  None),
            (1, [0],           list(slice_dict(small_symbols, 6, 7)),  dict(enumerate(small_blocks))),
        ]

        codec.sample = MagicMock(return_value=[0, 1, 2])
        decoder = codec.decoder()
        #print(codec.sample.mock_calls)

        for d, indices, symbols, expected in vector:
            codec.degree_from = MagicMock(return_value=d)
            codec.sample = MagicMock(return_value=indices)
            ret = decoder.send(dict(symbols))
            #print(codec.degree_from.mock_calls)
            #print(codec.sample.mock_calls)
            assert ret == expected

        with pytest.raises(StopIteration):
            decoder.send(None)  # Does not depend on object sent
