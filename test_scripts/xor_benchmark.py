# /usr/bin/python3
# coding: utf-8

"""
Benchmark on XOR implementations on multiple blocks
"""

from functools import reduce
from operator import xor
from timeit import timeit, repeat
from typing import Iterable, Sequence

import numpy as np
from numpy import fromstring, bitwise_xor, uint8, uint64


def xor_blocks(blocks:Sequence[bytes]) -> bytes:
    #assert all(len(blk) == len(blocks[0]) for blk in blocks[1:]), "All blocks must have equal length!"
    ret = bytearray(blocks[0])
    for blk in blocks[1:]:
        for i, b in enumerate(blk):
            ret[i] ^= b
    return ret

def xor_blocks2(blocks:Sequence[bytes]) -> bytes:  # Using numpy types and broadcasted functions: fast
    ret = fromstring(blocks[0], dtype=uint8)
    for blk in blocks[1:]:
        ret = bitwise_xor(ret, fromstring(blk, dtype=uint8))
    return ret.tostring()

def xor_blocks2b(blocks:Iterable[bytes]) -> bytes:  # Functional style: a bit faster
     return reduce(bitwise_xor, (fromstring(blk, dtype=uint8) for blk in blocks)).tostring()

def xor_blocks3(blocks:Sequence[bytes]) -> bytes:  # Bigger chunks: not faster than small chunks for 4KiB blocks
    ret = fromstring(blocks[0], dtype=uint64)
    for blk in blocks[1:]:
        ret = bitwise_xor(ret, fromstring(blk, dtype=uint64))
    return ret.tostring()

def xor_blocks3b(blocks:Iterable[bytes]) -> bytes:  # Functional style: a bit faster
    return reduce(bitwise_xor, (fromstring(blk, dtype=uint64) for blk in blocks)).tostring()

def xor_blocks4(blocks:Sequence[np.ndarray]) -> np.ndarray:  # Outer type conversion to numpy array: very fast
    ret = blocks[0]
    for blk in blocks[1:]:
        ret = bitwise_xor(ret, blk)
    return ret

def xor_blocks4b(blocks:Iterable[np.ndarray]) -> np.ndarray:  # Functional style: a bit faster
    return reduce(bitwise_xor, blocks)

def xor_blocks5(blocks:Iterable[bytes]) -> bytes:  # Using builtin big integers: fast
    ret = int.from_bytes(blocks[0], "little")
    for blk in blocks[1:]:
        ret ^= int.from_bytes(blk, "little")
    return ret.to_bytes(len(blocks[0]), "little")

def xor_blocks6(blocks:Sequence[int]) -> int:  # Outer type conversion to builtin big integers: very fast
    ret = blocks[0]
    for blk in blocks[1:]:
        ret ^= blk
    return ret

def xor_blocks6b(blocks:Iterable[int]) -> int:  # Functional style: not faster
    return reduce(xor, blocks)


def fmt_test_setup(*args, base_setup=""):
    return "from __main__ import {}".format(','.join(args)) + base_setup

def perf_test(stmt, setup="", number=1000):
    r = repeat(stmt, setup, number=number)
    t = min(r) / number

    fmt = (t, "s")
    if t < 1e-7: fmt = (t / 1e-9, "ns")
    elif t < 1e-4: fmt = (t / 1e-6, "µs")
    elif t < 1e-1: fmt = (t / 1e-3, "ms")
    t_fmt = "{} {}".format(round(fmt[0], 2), fmt[1])
    print(stmt, ':', t_fmt)

    return t


if __name__ == "__main__":

    blocks1 = [b'pticism they deserve as any proponent needs to explain paradoxical fact that most commercial WEB sites (which are actually a pretty complex software applications) are now driven by LAMP (or more correctly by WDS). Even Yahoo now uses PHP for the developmen',
    b"ge franchise, which (although it's a complex mix of informational and e-commerce sites) both in complexity and traffic requirements probably belongs to the top dozen of world e-commerce sites. Moreover the trends in hardware probably will help to preserve ",
    b"scripting languages dominance in WEB applications despite paradoxical inroad of Java on the server side in large enterprise environments as a new Cobol (and like was the case with Cobol, not without some help from IBM I think ;-) \n\nNow let's briefly discus"]

    blocks2 = [b"a" * 2**13] * 2**14

    blocks = blocks2

    print("n:", len(blocks), "bs:", len(blocks[0]))

    iterations = 10000 if blocks == blocks1 else 3

    print("\nTest preconversion\n")

    t0 = t = perf_test("[fromstring(blk, dtype=uint8) for blk in blocks]", fmt_test_setup("blocks", "fromstring", "uint8"), number=iterations)
    print(round(t0 / t, 2), "times faster")
    t = perf_test("[fromstring(blk, dtype=uint64) for blk in blocks]", fmt_test_setup("blocks", "fromstring", "uint64"), number=iterations)
    print(round(t0 / t, 2), "times faster")
    t = perf_test("[int.from_bytes(blk, 'little') for blk in blocks]", fmt_test_setup("blocks"), number=iterations)
    print(round(t0 / t, 2), "times faster")


    blocks_ndarrayuint8 = [fromstring(blk, dtype=uint8) for blk in blocks]
    blocks_ndarrayuint64 = [fromstring(blk, dtype=uint64) for blk in blocks]
    blocks_bigintle = [int.from_bytes(blk, "little") for blk in blocks]

    print("\nTest xor\n")

    t0 = t = perf_test("xor_blocks(blocks)", fmt_test_setup("xor_blocks", "blocks"), number=iterations)
    print(round(t0 / t, 1), "times faster")
    t = perf_test("xor_blocks2(blocks)", fmt_test_setup("xor_blocks2", "blocks"), number=iterations)
    print(round(t0 / t, 1), "times faster")
    t = perf_test("xor_blocks2b(blocks)", fmt_test_setup("xor_blocks2b", "blocks"), number=iterations)
    print(round(t0 / t, 1), "times faster")
    t = perf_test("xor_blocks3(blocks)", fmt_test_setup("xor_blocks3", "blocks"), number=iterations)
    print(round(t0 / t, 1), "times faster")
    t = perf_test("xor_blocks3b(blocks)", fmt_test_setup("xor_blocks3b", "blocks"), number=iterations)
    print(round(t0 / t, 1), "times faster")
    t = perf_test("xor_blocks4(blocks_ndarrayuint8)", fmt_test_setup("xor_blocks4", "blocks_ndarrayuint8"), number=iterations)
    print(round(t0 / t, 1), "times faster")
    t = perf_test("xor_blocks4b(blocks_ndarrayuint8)", fmt_test_setup("xor_blocks4b", "blocks_ndarrayuint8"), number=iterations)
    print(round(t0 / t, 1), "times faster")
    t = perf_test("xor_blocks4(blocks_ndarrayuint64)", fmt_test_setup("xor_blocks4", "blocks_ndarrayuint64"), number=iterations)
    print(round(t0 / t, 1), "times faster")
    t = perf_test("xor_blocks4b(blocks_ndarrayuint64)", fmt_test_setup("xor_blocks4b", "blocks_ndarrayuint64"), number=iterations)
    print(round(t0 / t, 1), "times faster")
    t = perf_test("xor_blocks5(blocks)", fmt_test_setup("xor_blocks5", "blocks"), number=iterations)
    print(round(t0 / t, 1), "times faster")
    t = perf_test("xor_blocks6(blocks_bigintle)", fmt_test_setup("xor_blocks6", "blocks_bigintle"), number=iterations)
    print(round(t0 / t, 1), "times faster")
    t = perf_test("xor_blocks6b(blocks_bigintle)", fmt_test_setup("xor_blocks6b", "blocks_bigintle"), number=iterations)
    print(round(t0 / t, 1), "times faster")

