# /usr/bin/python3
# coding: utf-8

"""
Reliable Unidirectional Transfer Protocol - prototype implementation

Protocol handling components
"""

import socket, struct
from math import ceil
import collections
from typing import MutableSequence, MutableMapping, Tuple, Text, Optional

from datatypes import ObjectMetadata, BlockMetadata, SymbolMetadata
from utils import tohex, fromhex, hexdump, hexdump2, hexdump3, hash256, crc32, pad_block, unpad_block


# PDU format parameters
PDU_BIN_FMT_BASE = ">IQH4s"
PDU_BIN_FMT_VAR = ">IQH4s{}s"
PDU_BIN_SIZE_BASE = struct.calcsize(PDU_BIN_FMT_BASE)
METADATA_BIN_FMT_BASE = ">IQ32sB"
METADATA_BIN_FMT_VAR = METADATA_BIN_FMT_BASE + "{}s"
METADATA_BIN_SIZE_BASE = struct.calcsize(METADATA_BIN_FMT_BASE)

# Protocol parameters
BLOCK_SIZE = 8000  # URI length = BLOCK_SIZE - 45
NET_BUFFER_SIZE = 8192
OBJECT_TYPES = ["raw", "file", "net"]


class Emitter:
    """Handles packet encapsulation and data emission.
    
    Packet format is specified in function build_PDU.
    """
    # TODO: have it handle packets asynchronously

    def __init__(self, ip: Text, port: int) -> None:
        self.dest_address = (ip, port)  # type: Tuple[Text, int]
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # type: socket.socket
        # Explicit bind not recommended for client sockets, done implicitely by connect or sendto
        # Connect sets a specific destination as default, no info is transmitted on the link
        self.sock.connect(self.dest_address)

    def process(self, symbol: SymbolMetadata):
        """Pack protocol metadata, including per-packet integrity code into a datagram and sends it."""
        symbol.crc = crc32(symbol.data).to_bytes(4, byteorder="big")
        data = self.build_PDU(symbol)
        print("\n<Emitter> Outgoing PDU:", len(data), "bytes")
        print(hexdump2(data))
        self.sock.send(data) # send because we connect'ed before

    @classmethod
    def build_PDU(cls, symbol: SymbolMetadata) -> bytes:
        """Packs PDU data according to the binary format of RUTP:
        - OID as uint32
        - SID as uint64
        - data_length as uint16
        - data as byte[]
        - CRC-32 as byte[4]
        """
        data = symbol.data
        return struct.pack(PDU_BIN_FMT_VAR.format(len(data)), symbol.oid, symbol.sid, len(data), symbol.crc, data)

class Receiver:
    """Handles packet reception and packet decapsulation."""
    # TODO: have it handle packets asynchronously

    def __init__(self, ip: Text, port: int) -> None:
        self.server_address = (ip, port)  # type: Tuple[Text, int]
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # type: socket.socket
        # Explicit bind nedded for servers
        self.sock.bind(self.server_address)

        #self.incomplete_objects = {}  # type: MutableMapping[Text, ObjectMetadata]
        #self.complete_objects = collections.deque()  # type: MutableSequence[ObjectMetadata]

    def shutdown(self):
        """Terminates reception by closing sockets."""
        self.sock.close()
        
    def process(self) -> Optional[SymbolMetadata]:
        """Recieves a datagram and unpacks protocol metadata. Verifies per-packet integrity code.
Returns a symbol or None if no valid data has been recieved."""
        # Receive data
        data, addr = self.sock.recvfrom(NET_BUFFER_SIZE)
        if not data: return None
        print("\n<Receiver> Ingoing PDU:", len(data), "bytes")
        print(hexdump2(data))

        # Dissect and validate
        symbol = self.dissect_PDU(data)
        if not self.verifyCRC(symbol):
            return None

        return symbol
        
    @classmethod
    def dissect_PDU(cls, pdu: bytes) -> SymbolMetadata:
        """Unpacks PDU data according to the binary format of RUTP:
        - OID as uint32
        - SID as uint64
        - data_length as uint16
        - data as byte[]
        - CRC-32 as byte[4]
        """
        # Using big endian
        # Here we included the block length, but we could compute it from struct.calcsize()
        oid, sid, data_len, crc = struct.unpack(PDU_BIN_FMT_BASE, pdu[:PDU_BIN_SIZE_BASE])
        data = pdu[PDU_BIN_SIZE_BASE: PDU_BIN_SIZE_BASE + data_len]
        return SymbolMetadata(sid=sid, oid=oid, size=data_len, data=data, crc=crc)

    def verifyCRC(self, symbol:SymbolMetadata) -> bool:
        """Returns whether data is valid according to CRC-32 integrity code."""
        ok = True
        computed_crc = crc32(symbol.data).to_bytes(4, byteorder="big")
        print("<Receiver> CRC", ("OK" if symbol.crc == computed_crc else "ERROR!"), computed_crc, "(computed)")
        if symbol.crc != computed_crc:
            print("<Receiver> Packet dropped.")
            ok = False
        del symbol.crc
        return ok


class FECEncoder:
    """Encodes data using a Forward Error Correcting scheme.
Default implementation is a dummy encoding."""
    # TODO: use real correcting code, with a switch to (de)activate dummy mode

    def process_dummy(self, fobj: ObjectMetadata) -> ObjectMetadata:
        """Performs dummy encoding process that does not transform data. Returns `fobj` with attribute `symbols` filled with dummy symbols corresponding to original source blocks."""
        symbols = [SymbolMetadata(blk.bid, blk.oid, BLOCK_SIZE, blk.data) for i, blk in enumerate(fobj.blocks)]
        #print("\n<FEC Encoder> Symbols:", symbols)
        fobj.symbols = symbols
        # sid = len(fobj.symbols)
        # bid = sid % len(fobj.blocks)
        # chosen_block = fobj.blocks[bid]
        # if bid == 0 or bid == len(fobj.blocks)-1:
        # chosen_block.data = pad_block(chosen_block.data)
        # symbol = SymbolMetadata(sid, fobj.oid, [bid], BLOCK_SIZE, chosen_block.data)
        # fobj.symbols.append(symbol)
        # print("Symbol:", symbol)
        return fobj

    def process_online(self, fobj: ObjectMetadata) -> ObjectMetadata:
        pass

    process = process_dummy

class FECDecoder:
    """Decodes data using a Forward Error Correcting scheme.
Default implementation is a dummy decoding."""
    # TODO: use real correcting code

    def process(self, fobj: ObjectMetadata) -> ObjectMetadata:
        """Performs one step of a dummy decoding process that does not transform data. Returns `fobj` with attribute `blocks` filled with source blocks corresponding to dummy symbols."""
        sbl = fobj.symbols.pop()
        blocks = [BlockMetadata(bid=sbl.sid, oid=sbl.oid, priority=0, data=sbl.data)]
        #print("<FEC Decoder> Retrieved blocks:", [(b.oid, b.bid) for b in blocks])

        fobj.blocks.extend(blocks)

        return fobj


class Segmenter:
    """Segments source object into fixed-size blocks."""

    def process(self, fobj: ObjectMetadata) -> ObjectMetadata:
        """Handles serialization, segmentation and padding on data to produce source blocks. Returns `fobj` with attribute `blocks` filled with source blocks created from objects' source data."""
        # Serialize metadata
        metadata = self.serialize_metadata(fobj)
        
        # Cut the blocks
        obj_data = fobj.data
        blocks_bytes = [obj_data[i * BLOCK_SIZE: i * BLOCK_SIZE + BLOCK_SIZE] for i in
                        range(int(ceil(len(obj_data) / BLOCK_SIZE)))]

        # Pad the two non-normalized blocks
        metadata = pad_block(metadata, BLOCK_SIZE)
        blocks_bytes[-1] = pad_block(blocks_bytes[-1], BLOCK_SIZE)

        # Create block objects
        blocks = [BlockMetadata(data=metadata, bid=0, oid=fobj.oid, priority=fobj.priority)]
        blocks.extend(BlockMetadata(data=block, bid=i + 1, oid=fobj.oid, priority=fobj.priority)
                      for i, block in enumerate(blocks_bytes))
        
        print("\n<Segmenter> Blocks:", blocks)
        fobj.blocks = blocks
        return fobj

    @classmethod
    def serialize_metadata(cls, fobj: ObjectMetadata) -> bytes:
        """
        Pack object metadata in the following binary format:
        - OID as uint32
        - object_size as uint64
        - hash as byte[32]
        - URI_length as uint8
        - URI as a byte[]
        """
        # Gotta pack 'em all!
        return struct.pack(METADATA_BIN_FMT_VAR.format(len(fobj.uri)), fobj.oid,
            fobj.size, fobj.hash, len(fobj.uri), cls.serialize_URI(fobj.uri))
    
    @classmethod
    def serialize_URI(cls, oid: Text) -> bytes:
        return oid.encode('utf8')

class Unsegmenter:
    """Reassembles source blocks into source object."""

    def process(self, fobj: ObjectMetadata) -> Optional[ObjectMetadata]:
        """Handles deserialization, reassembly and unpadding on source blocks to recover source object. Returns `fobj` with attribute `data` filled with source data created from objects' source blocks."""
        # Retrieving metadata if received yet
        print("<Unsegmenter> Checking retrieved blocks...")
        meta_blk = next((blk for blk in fobj.blocks if blk.bid == 0), None)
        if not meta_blk: return None
        
        metadata = self.deserialize_metadata(meta_blk.data)  # no need unpadding with this
        block_count = ceil(metadata.size / BLOCK_SIZE) + 1  # 1 for metadata
        block_group = list(set(fobj.blocks))
        print("<Unsegmenter> Block count (incl metadata) [retrieved / required]:", len(fobj.blocks), '/', block_count)

        # Test whether we can reassemble blocks into an object
        # Are this object's blocks all retrieved?
        if len(block_group) < block_count: return None
        if len(block_group) > block_count:
            print("<Unsegmenter> Too many blocks for object ", metadata.oid)
            raise ValueError("Too many blocks for object %s" % metadata.oid)

        # Need ordering to index blocks
        block_group.sort(key=lambda e: e.bid)
        last_block = fobj.blocks[-1]
        # Handles case of no padding
        last_block.data = unpad_block(last_block.data, (metadata.size % BLOCK_SIZE) or BLOCK_SIZE)

        # Build the object metadata
        block_group.remove(meta_blk)
        fobj.blocks = block_group
        for attr in ("oid", "size", "uri", "hash"):
            setattr(fobj, attr, getattr(metadata, attr))
    
        # Reconstruct the object data
        data = b''.join(blk.data for blk in fobj.blocks)
        del fobj.blocks
        fobj.data = data
        
        return fobj

    @classmethod
    def deserialize_metadata(cls, fobj_bytes: bytes) -> ObjectMetadata:
        """Deserialize RUTP metadata packed into a buffer. No need unpadding first."""
        oid, size, hash_, uri_len = struct.unpack(METADATA_BIN_FMT_BASE, fobj_bytes[:METADATA_BIN_SIZE_BASE])
        uri = cls.deserialize_URI(fobj_bytes[METADATA_BIN_SIZE_BASE: METADATA_BIN_SIZE_BASE + uri_len])
        return ObjectMetadata(oid=oid, size=size, uri=uri, data=None, hash_=hash_, priority=0)

    @classmethod
    def deserialize_URI(cls, uri_bytes: bytes) -> Text:
        return uri_bytes.decode('utf8')


class Formatter:
    """Formats object with metadata for use in the application. Computes object hash."""

    current_id = 0

    @classmethod
    def gen_id(cls):
        """Returns a unique id taken from an infinite counter."""
        cls.current_id += 1
        return cls.current_id

    def process(self, uri: Text, obj: bytes, priority: int) -> ObjectMetadata:
        """Format given data into a formatted object structure."""
        oid = self.gen_id()
        return ObjectMetadata(oid=oid, uri=uri, size=len(obj), priority=priority, data=obj, hash_=hash256(obj))


class Unformatter:
    """Removes object formatting and verifies object hash."""

    def process(self, fobj: ObjectMetadata) -> ObjectMetadata:
        # Check the hash
        if not self.verifyHash(fobj):
            return None
    
        # No unformatting without knowing which output format
    
        return fobj
        
    def verifyHash(self, fobj:ObjectMetadata) -> bool:
        """Returns whether data is valid according to a 256-bit hash integrity code."""
        ok = True
        computed_hash = hash256(fobj.data)
        print("\n<Unformatter> Hash", "OK" if computed_hash == fobj.hash else "ERROR!")
        print(tohex(computed_hash).decode("ascii"), "(computed)")
        if computed_hash != fobj.hash:
            print("<Unformatter> Object is corrupted!")
            ok = False
        del fobj.hash
        return ok

