# /usr/bin/python3
# coding: utf-8

"""
Reliable Unidirectional Transfer Protocol - prototype implementation

Application utility functions
"""

from math import ceil
import itertools
import binascii
import hashlib
from typing import Text, Iterable, Any

_debug = True
def debug(*args, **kwargs):
    if _debug: print(*args, **kwargs)

tohex = lambda b: binascii.hexlify(b).decode('ASCII')
fromhex = lambda s: binascii.unhexlify(s.encode('UTF-8'))

def hexdump(data:bytes) -> Text:
    """Classical hexdump without unicode interpretation"""
    return '\n'.join(' '.join("{:02X}".format(b) for b in data[i*16:i*16+16]) for i in range(int(ceil(len(data)/16))))

def hexdump2(data:bytes) -> Text:
    """Classical hexdump with unicode interpretation"""
    return '\n'.join("%-48s" %
            ''.join("{:02X} ".format(b) for b in data[i*16:i*16+16]) +
            ''.join(c if c.isprintable() else '.' for c in data[i*16:i*16+16].decode('utf8', 'replace'))
            for i in range(int(ceil(len(data)/16))))

def hexdump3(data:bytes) -> Text:
    """Hexdump that alterns lines of hex and unicode representations"""
    return '\n'.join( \
            ''.join("{:02X} ".format(b) for b in data[i*16:i*16+16]) + '\n' +
            ''.join("{:>2} ".format(chr(b)) for b in data[i*16:i*16+16])
            for i in range(int(ceil(len(data)/16))))

def hexdump4(data:bytes, block_len:int=4) -> Text:
    """Compact hexdump without unicode interpretation"""
    B = block_len
    N = 80 // (B*2+1)
    L = N * B
    return '\n'.join(' '.join(tohex(data[i*L+j*B:i*L+j*B+B]) for j in range(N)) for i in range(ceil(len(data)/L)))

def blocks_summary(blocks:dict):
    debug("Block summary:", len(blocks))
    debug('\n…\n'.join("Block %s\n%s …" % (bid, hexdump4(blk[:64], 4)) for bid, blk in slice_dict(blocks, 0, len(blocks), len(blocks)-1)))

def show_bytes(data:Iterable[Any]) -> Text:
    buf = bytearray()
    for dat in data:
        if isinstance(dat, int): buf.extend(dat.to_bytes(4, 'big'))
        elif isinstance(dat, str): buf.extend(bytes(dat, 'UTF-8'))
        elif isinstance(dat, (bytes, bytearray)): buf.extend(dat)
        else: buf.extend(bytes(str(dat), 'UTF-8'))
    return tohex(buf)

def convert(val, convert_func, default=None):
    """Simlifies data conversion by catching conversion failures and providing a default value"""
    try:
        return convert_func(val)
    except (ValueError, TypeError):
        return default

def slice_dict(dic:dict, start, stop=None, step=None):
    """Slices a dict iterator from start (def. to 0) to stop with step"""
    return itertools.islice(dic.items(), start, stop, step if step > 0 else None)

def chunked(n, iterable, pad=None, _islice=itertools.islice):
    """Segments an iterator into fixed-sized chunks, using a specific padding value"""
    it = iter(iterable)
    if pad is None:
        return iter(lambda: list(_islice(it, n)), [])
    else:
        return itertools.zip_longest(*[it]*n, fillvalue=pad)

def hash256(data: bytes) -> bytes:
    """Hash data with a 256-bit hash algorithm"""
    return hashlib.sha256(data).digest()

def crc32(data: bytes) -> int:
    """Hash data with a 32-bit CRC algorithm"""
    return binascii.crc32(data)

def pad_block(block: bytes, size: int) -> bytes:  # TODO: Change padding to 0x80, and use a padding flag in PDU
    """Pads a block of data up to `size` bytes with a null-byte padding scheme"""
    return block + (size - len(block)) * b'\0'

def unpad_block(block: bytes, size: int) -> bytes:
    """Unpads a block of data down to `size` bytes"""
    return block[:size]
