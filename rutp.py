# /usr/bin/python3
# coding: utf-8

"""
Reliable Unidirectional Transfer Protocol - prototype implementation

"""

import sys
from typing import MutableSequence, MutableMapping, Dict, Text, Optional
import collections

from components import Emitter, Receiver, Formatter, Unformatter, Segmenter, Unsegmenter, FECEncoder, FECDecoder
from datatypes import ObjectMetadata, BlockMetadata, SymbolMetadata
from utils import convert, hexdump, hexdump2, hexdump3

PORT = 4000


class Client:
    """RUTP emitter."""

    def __init__(self, ip:Text, port:int=4000) -> None:
        self.formatter = Formatter()
        self.segmenter = Segmenter()
        self.fec_encoder = FECEncoder()
        self.emitter = Emitter(ip, port)
        print("<Client> Started on {}:{}".format(ip, port))

    def submit(self, uri:Text="raw", obj:bytes=None, priority:int=0):
        assert any((uri, obj)), "You must provide at least URI or raw data"
        print("<Client> Submitted object size:", len(obj))

        fobj = self.formatter.process(uri, obj, priority)

        fobj = self.segmenter.process(fobj)

        fobj = self.fec_encoder.process(fobj)

        # Send data mixed with metadata
        for symbol in fobj.symbols:
            self.emitter.process(symbol)
        print("\n<Client> Sent {} packets\n\n---\n".format(len(fobj.symbols)))

class Server:
    """RUTP reciever."""

    def __init__(self, ip, port=4000):
        self.unformatter = Unformatter()
        self.unsegmenter = Unsegmenter()
        self.fec_decoder = FECDecoder()
        self.receiver = Receiver(ip, port)
        print("<Server> Started on {}:{}".format(ip, port))

        self.incomplete_objects = {}  # type: MutableMapping[Text, ObjectMetadata]
        self.complete_objects = collections.deque()  # type: MutableSequence[ObjectMetadata]

    def run(self):
        try:
            while True:
                symbol = self.receiver.process()
                
                # Get corresponding object or create it
                fobj = self.incomplete_objects.setdefault(symbol.oid, ObjectMetadata(oid=symbol.oid))
                fobj.symbols.append(symbol)

                fobj = self.fec_decoder.process(fobj)

                fobj = self.unsegmenter.process(fobj)
                if not fobj: continue
                
                # Object is now completed
                self.complete_objects.append(fobj)
                del self.incomplete_objects[fobj.oid]

                fobj = self.unformatter.process(fobj)
                print("\n<Server>Processed object:\n", fobj)
        finally:
            self.receiver.shutdown()

### Main ##################################################################


def process_command_line():
    argv = sys.argv
    if len(argv) < 4:
        print("Usage: python3 rutp.py client|server <ip> <port>")
        return

    ip = argv[2]
    port = convert(argv[3], int, PORT)

    if argv[1] == "client":
        client = Client(ip, port)
        while True:
            data = input().encode("utf8")
            client.submit(obj=data, uri="raw", priority=2)
    elif argv[1] == "server":
        Server(ip, port).run()

if __name__ == '__main__':
    process_command_line()
